#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import re, docker

from plugins.abstract.abstract_containers_plugins import AbstractContainersPlugin
from include.validate import validate_keys

class DockerPlugin(AbstractContainersPlugin):
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractContainersPlugin.__init__(self, "dr", "docker", current_instance, name, configuration, debug_enabled)
        self.do_renameInterface=True
        self.client_docker=docker.from_env()
        self.container=None
        self.internal_keys.extend(('environment', 'user'))

    def execInContainer(self, cmd, wait=True, ignore_errors=False):
        if not self.containerStarted:
            return
        self.debug("DockerPlugin:execInContainer : {}".format(" ".join(cmd)))
        exit_code=0
        try:
            std=self.container.exec_run(cmd, detach=not wait)
            # fix https://github.com/docker/docker-py/issues/1381
            if isinstance(std, bytes) and b"runtime exec failed" in std:
                exit_code=1
                raise Exception(std.decode())
            elif isinstance(std, tuple) and len(std)==2:
                exit_code, output=std
                if exit_code!=0:
                    raise Exception("Command return code error (!=0) code={}, stderr='{}'".format(exit_code, output.decode()[:160]))
        except Exception as e:
            if not ignore_errors:
                raise Exception(e)
        return exit_code

    def createContainer(self, image):
        kwargs = {
            "image": image,
            "name": self.current_container_name,
            "auto_remove": True,
            "cap_add": ["NET_ADMIN"]
        }
        if "user" in self.configuration:
            kwargs["user"]=self.configuration["user"]
        if "environment" in self.configuration:
            environments=self.configuration['environment']
            if environments and isinstance(environments, list):
                kwargs["environment"]=environments
            else: raise Exception("a list is required (environment)")
        if "quotas" in self.configuration:
            kwargs.update(self.getQuotasArgs(self.configuration["quotas"]))
        if "command" in self.configuration:
            command=self.configuration['command']
            if command and isinstance(command, list):
                    kwargs["command"]=command
            else: raise Exception("a list is required (command)")
        self.debug(f"container.create {kwargs}")
        self.container=self.client_docker.containers.create(**kwargs)
        self.containerCreated=True

    def startContainer(self):
        try:
            self.container.start()
            self.containerStarted=True
        except Exception as err:
            self.containerStarted=False
            raise Exception("Impossible to start docker container ({0}) : {1}".format(self.current_container_name, err))

    def isContainerExist(self):
        try:
            self.client_docker.containers.get(self.current_container_name)
            return True
        except:
            pass
        return False

    def destroyContainer(self):
        if not self.containerStarted and self.containerCreated:
            # created but never started -> destroy
            try:
                self.container.remove()
            except:
                pass
            return
        if self.containerStarted:
            try:
                self.container.stop(timeout=0)
            except:
                pass
        # fix if stopped and not deleted
        if self.isContainerExist():
            try:
                self.container.remove(force=True)
            except:
                pass
        self.containerCreated=False
        self.containerStarted=False

    def pushFileInContainer(self, src, dst):
        if self.containerCreated:
            cmd=('docker', 'cp', src, f"{self.current_container_name}:{dst}")
            self.execCommand(cmd)

    def restartContainer(self):
        if self.containerStarted:
            # move interface in default network namespace before, otherwise it will be lost ...
            cmds=[]
            for configuration in self.configuration["interfaces"]:
                link, intname = self.getLinkAndIntNameFromConfig(configuration)
                cmds.append(self.commands.setDownInterface(intname))
                cmds.append(self.commands.renameInterface(intname, link))
                cmds.append(self.commands.attachIntToNetns(link, '1')) # to default netns
            for cmd, wait, _ in cmds:
                self.execCommandonNetns(self.current_container_name, cmd, wait,  ignore_error=True)
            self.container.restart(timeout=0)

    def getContainerPid(self):
        if self.currentPid:
            return self.currentPid
        try:
            client_low_api = docker.APIClient()
            self.currentPid=int(client_low_api.inspect_container(self.container.id)["State"]["Pid"])
            return self.currentPid
        except:
            pass
        raise Exception("Impossible to get PID of {}".format(self.current_container_name))

    def attachInterfaceInContainerStage1(self, link, intname):
        pass

    def attachInterfaceInContainerStage2(self, link, intname):
        # attach interface in net namespace
        self.execCommand(('ip', 'link', 'set', link, 'netns', self.current_container_name))

    def getSuffixFromBytes(self, _bytes):
        return re.sub(r'([a-z,A-Z])(B|o)$', r'\g<1>', str(_bytes)).lower()

    def getQuotasArgs(self, quotas):
        validate_keys(('disk', 'cpus', 'cpuset-cpus', 'memory', 'memory-swap'), quotas)
        cmd={}
        if "disk" in quotas:
            disk=self.getSuffixFromBytes(quotas["disk"])
            cmd["storage_opt"]={"size": disk}
        if "cpus" in quotas:
            cmd["cpu_period"]=100000
            cmd["cpu_quota"]=int(quotas["cpus"]*100000.0)
        if "cpuset-cpus" in quotas:
            cmd["cpuset_cpus"]=quotas["cpuset-cpus"]
        if "memory" in quotas:
            memory=self.getSuffixFromBytes(quotas["memory"])
            cmd["mem_limit"]=memory
        if "memory-swap" in quotas:
            memory_swap=self.getSuffixFromBytes(quotas["memory-swap"])
            cmd["memswap_limit"]=memory_swap
        return cmd


pluginName=DockerPlugin
