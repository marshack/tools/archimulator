#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os, subprocess
import time, ipaddress
from include.validate import validate_keys

class _wait():
    yes=True
    no=False

class _ignore_error():
    yes=True
    no=False

class ConfigureCommands():
    def createNetns(self, netns):
        return ('ip', 'netns', 'add', netns), _wait.yes, _ignore_error.no
    def deleteNetns(self, netns):
        return ('ip', 'netns', 'del', netns), _wait.yes, _ignore_error.no
    def executeInNetns(self, netns):
        return ('ip', 'netns', 'exec', netns), _wait.yes, _ignore_error.no
    def attachIntToNetns(self, interface, netns):
        return ('ip', 'link', 'set', interface, 'netns', netns), _wait.yes, _ignore_error.no
    def renameInterface(self, source_int, target_name):
        return ('ip', 'link', 'set', source_int, 'name', target_name), _wait.yes, _ignore_error.no
    def setHwAddrInterface(self, interface, hw_address):
        return ('ip', 'link', 'set', 'dev', interface, 'address', hw_address), _wait.yes, _ignore_error.no
    def setUpInterface(self, interface):
        return ('ip', 'link', 'set', 'dev', interface, 'up'), _wait.yes, _ignore_error.no
    def setDownInterface(self, interface):
        return ('ip', 'link', 'set', 'dev', interface, 'down'), _wait.yes, _ignore_error.no
    def setIpAddress(self, interface, address):
        return ('ip', 'addr', 'add', address, 'dev', interface), _wait.yes, _ignore_error.no
    def setDhcp4(self, interface):
        return ('udhcpc', '-b', '-n', '-q', '-i', interface, '-s', os.path.join(os.path.dirname(os.path.abspath(__file__)), "udhcpc/default.script")), _wait.no, _ignore_error.no
    def acceptRA(self, interface):
        return ('sysctl', '-w', f'net.ipv6.conf.{interface}.accept_ra=1'), _wait.yes, _ignore_error.no
    def doNotRespondToRA(self, interface):
        return ('sysctl', '-w', f'net.ipv6.conf.{interface}.accept_ra=0'), _wait.yes, _ignore_error.no
    def setDefaultRoute(self, address):
        return ('ip', 'route', 'add', 'default', 'via', "{}".format(address)), _wait.yes, _ignore_error.no
    def deleteDefaultRoute(self):
         return ('ip', 'route', 'delete', 'default'), _wait.yes, _ignore_error.yes
    def addRoute(self, to, via):
        return ('ip', 'route', 'add', to, 'via', via), _wait.yes, _ignore_error.no
    def enableForwarding(self):
        return ('sysctl', '-w', 'net.ipv4.ip_forward=1'), _wait.yes, _ignore_error.no
    def disableForwarding(self):
        return ('sysctl', '-w', 'net.ipv4.ip_forward=0'), _wait.yes, _ignore_error.no
    def enableForwarding6(self):
        return ('sysctl', '-w', 'net.ipv6.conf.all.forwarding=1'), _wait.yes, _ignore_error.no
    def disableForwarding6(self):
        return ('sysctl', '-w', 'net.ipv6.conf.all.forwarding=0'), _wait.yes, _ignore_error.no
    def enableRouteLocalHost(self):
        return ('sysctl', '-w', 'net.ipv4.conf.all.route_localnet=1'), _wait.yes, _ignore_error.no
    def disableRouteLocalHost(self):
        return ('sysctl', '-w', 'net.ipv4.conf.all.route_localnet=0'), _wait.yes, _ignore_error.no
    def isBridgeInterface(self, interface):
        return ('ip', 'link', 'show', 'type', 'bridge', interface), _wait.yes, _ignore_error.no
    def addIntInVlan(self, interface, vlan):
        return ('bridge', 'vlan', 'add', 'dev', interface, 'vid', vlan), _wait.yes, _ignore_error.no
    def removeIntInVlan(self, interface, vlan, _type):
        return ('bridge', 'vlan', 'del', 'dev', interface, 'vid', vlan, _type), _wait.yes, _ignore_error.no
    def removeInterface(self, interface):
        return ('ip', 'link', 'delete', interface), _wait.yes, _ignore_error.no
    def enableMasquerade(self, interface, from_source):
        return ('iptables', '-t', 'nat', '-A', 'POSTROUTING', \
            '-o', interface, '-s', from_source, '-j', 'MASQUERADE'), _wait.yes, _ignore_error.no
    def enableMasquerade6(self, interface, from_source):
        return ('ip6tables', '-t', 'nat', '-A', 'POSTROUTING', \
            '-o', interface, '-s', from_source, '-j', 'MASQUERADE'), _wait.yes, _ignore_error.no

class AbstractSystem():
    def __init__(self, debug_enabled=False):
        self.debug_enabled=debug_enabled
        self.netns_collection=[]
        self.proc_collection=[]
        self.interfaces_collection=[]
        self.commands=ConfigureCommands()
        self.interface_valid_keys= ( 'intname', 'addresses', 'link' , 'gw', 'mac', 'dhcp4', 'accept-ra' )
        self.route_valid_keys = ('forward', 'nat', 'snat', 'dnat', 'static' , 'gw' )

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def printError(self, msg):
        sys.stderr.write("ERROR - {}\n".format(msg))

    def execCommand(self, cmd, wait=True, ignore_errors=False):
        self.debug('AbstractSystem:execCommand - {}'.format(" ".join(cmd)))
        stdout=[]
        p=subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.proc_collection.append(p)
        if not wait:
            return 0, []
        else:
            returncode=p.wait()
            stdout=p.stdout.read().decode()
            if returncode!=0:
                stderr=p.stderr.read().decode()
                if len(stderr)==0:
                    stderr=stdout # error message is in stdout
                stderr=stderr.replace('\n', ' ').strip()[0:80] # clean and limit size
                if not ignore_errors:
                    raise Exception(stderr)
                else:
                    self.debug("Warning - {}".format(stderr))
        if p in self.proc_collection:
            self.proc_collection.remove(p)
        return 0, stdout.split('\n')

    def killProcesses(self):
        if not self.proc_collection:
            return
        self.debug("AbstractSystem:killProcesses")
        # terminate
        for p in self.proc_collection:
            if p.returncode==None:
                self.debug("try to terminate pid {} ({})".format(p.pid, " ".join(p.args)))
                try:
                    p.terminate()
                    p.wait(0.4)
                except:
                    pass
        time.sleep(0.5)
        # kill
        for p in self.proc_collection:
            if p.returncode==None:
                self.debug("is alive, kill pid {} ({})".format(p.pid, " ".join(p.args)))
                try:
                    p.kill()
                    p.wait(0.2)
                except:
                    pass
        self.proc_collection.clear()

    def isIPv4(self, addr):
        try:
            ipaddress.IPv4Address(addr)
        except:
            return False
        return True

    def isIPv6(self, addr):
        try:
            ipaddress.IPv6Address(addr)
        except:
            return False
        return True

    def isNetIPv4(self, subnet):
        try:
            ipaddress.IPv4Network(subnet)
        except:
            return False
        return True

    def isNetIPv6(self, subnet):
        try:
            ipaddress.IPv6Network(subnet)
        except:
            return False
        return True

    def isNetnsExist(self, netns):
        return netns in self.netns_collection

    def createNetnsIfnotexist(self, netns):
        if not self.isNetnsExist(netns):
            self.execCommand(self.commands.createNetns(netns)[0])
            self.netns_collection.append(netns)
            # up loopback
            self.setUpInterface("lo", netns)

    def execCommandonNetns(self, netns, cmd, wait=True, ignore_error=False):
        new_cmd=[]
        if netns:
            self.createNetnsIfnotexist(netns)
            new_cmd=list(self.commands.executeInNetns(netns)[0])
        new_cmd.extend(cmd)
        self.execCommand(new_cmd, wait, ignore_error)

    def attachIntToNetns(self, interface, netns):
        self.createNetnsIfnotexist(netns)
        self.execCommand(self.commands.attachIntToNetns(interface, netns)[0])

    def setUpInterface(self, interface, netns=None):
        self.execCommandonNetns(netns, self.commands.setUpInterface(interface)[0])

    def renameInterface(self, source_int, target_name, netns=None):
        if source_int!=target_name:
            self.execCommandonNetns(netns, self.commands.renameInterface(source_int, target_name)[0])

    def setHwAddrInterface(self, interface, hw_address, netns=None):
        self.execCommandonNetns(netns, self.commands.setHwAddrInterface(interface, hw_address)[0])

    def setIpAddress(self, interface, address, netns=None):
        self.execCommandonNetns(netns, self.commands.setIpAddress(interface, address)[0])

    def setDhcp4(self, interface, netns=None):
        self.execCommandonNetns(netns, self.commands.setDhcp4(interface)[0], wait=False)

    def setDefaultRoute(self, address, netns=None):
        self.execCommandonNetns(netns, self.commands.setDefaultRoute(address)[0])

    def setVlanOnInterface(self, interface, vlan, is_tagged=False, is_bridge=False,  netns=None):
        _type="self" if is_bridge==True else "master"
        cmd=list(self.commands.addIntInVlan(interface, vlan)[0])
        if not is_tagged:
            cmd.extend(('pvid', 'untagged', _type))
        else:
            cmd.extend(('tagged', _type))
        self.execCommandonNetns(netns, cmd)

    def removeVlanOnInterface(self, interface, vlan, is_bridge=False,  netns=None):
        _type="self" if is_bridge==True else "master"
        self.execCommandonNetns(netns, self.commands.removeIntInVlan(interface, vlan, _type)[0])

    def deleteAllNetns(self):
        if not self.netns_collection:
            return
        self.debug('AbstractSystem:deleteAllNetns')
        for netns in self.netns_collection:
            self.debug("Delete net namespace : {}".format(netns))
            try:
                self.execCommand(self.commands.deleteNetns(netns)[0])
            except:
                self.printError("Impossible to remove net namespace : {}".format(netns))
                pass
        self.netns_collection.clear()

    def isBridgeInterface(self, interface):
        p=subprocess.Popen(self.commands.isBridgeInterface(interface)[0], \
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        returncode=p.wait()
        if returncode!=0:
            return False
        stdout=p.stdout.read().decode().replace('\n', ' ')
        if interface in stdout:
            return True
        return False

    def removeInterfaces(self):
        if not self.interfaces_collection:
            return
        self.debug('AbstractSystem:removeInterfaces')
        for interface in self.interfaces_collection:
            try:
                self.execCommand(self.commands.removeInterface(interface)[0])
            except:
                pass

    def getCmdsToConfigureInterface(self, configuration, interface, rename_interface=True):
        validate_keys(self.interface_valid_keys, configuration)
        cmds=[]
        origin_name=interface
        if "intname" in configuration:
            interface=configuration["intname"]
        elif "link" in configuration:
            interface=configuration["link"]

        if origin_name!=interface and rename_interface:
            # rename interface
            cmds.append(self.commands.renameInterface(origin_name, interface))

        cmds.append(self.commands.setUpInterface(interface))

        if "mac" in configuration:
            hw_address=configuration["mac"]
            cmds.append(self.commands.setHwAddrInterface(interface, hw_address))

        if "dhcp4" in configuration and configuration["dhcp4"]:
            cmds.append(self.commands.setDhcp4(interface))

        if "accept-ra" in configuration:
            if configuration["accept-ra"]:
                cmds.append(self.commands.acceptRA(interface))
            else:
                cmds.append(self.commands.doNotRespondToRA(interface))

        if "addresses" in configuration:
            addresses=configuration["addresses"]
            if not isinstance(addresses, list):
                raise Exception("No addresses found (it's not a list)")

            for address in addresses:
                cmds.append(self.commands.setIpAddress(interface, address))

        # for convenience
        if "gw" in configuration:
            gw=configuration["gw"]
            cmds.append(self.commands.deleteDefaultRoute())
            cmds.append(self.commands.setDefaultRoute(gw))
        return cmds

    def getCmdsToConfigureRoutes(self, configuration_routes):
        validate_keys(self.route_valid_keys, configuration_routes)
        cmds=[]
        if "nat" in configuration_routes:
            configuration_nat=configuration_routes["nat"]

            if isinstance(configuration_nat, dict):
                configuration_nat=[configuration_nat]

            for nat in configuration_nat:
                validate_keys(('source', 'out'), nat)
                addresses=nat["source"]
                out=nat["out"]

                if not isinstance(addresses, list):
                    raise Exception("No nat configuration found (it's not a list)")
                for source in addresses:
                    if self.isNetIPv4(source):
                        cmds.append(self.commands.enableMasquerade(out, source))
                    elif self.isNetIPv6(source):
                        cmds.append(self.commands.enableMasquerade6(out, source))
                    else:
                        raise Exception(f"Subnet is invalid : {source}")

        if "static" in configuration_routes:
            static=configuration_routes["static"]
            if isinstance(static, dict):
                static=[static]
            for s in static:
                validate_keys(('to', 'via', 'dev', 'metric'), s)
                cmd=['ip', 'route', 'add', s["to"]]
                if "via" in s :
                    cmd.extend(('via', s["via"]))
                if "dev" in s :
                    cmd.extend(('dev', s["dev"]))
                if "metric" in s:
                    cmd.extend(('metric', str(s["metric"])))

                cmds.append((cmd, _wait.yes, _ignore_error.no))

        if "dnat" in configuration_routes:
            dnat=configuration_routes["dnat"]
            if isinstance(dnat, dict):
                dnat=[dnat]
            for s in dnat:
                validate_keys(('in', 'protocole', 'dports', 'to' ,'source'), s)
                prefix=['iptables', '-t', 'nat', '-A', 'PREROUTING']
                suffix=[]
                if 'in' in s:
                    _in=s['in']
                    if isinstance(_in, list):
                        _in=','.join(_in)
                    suffix.extend(('-i', _in))
                if 'protocole' in s: suffix.extend(('-p', s['protocole']))
                if 'dports' in s:
                    dports=s['dports']
                    if isinstance(dports, list):
                        dports= ','.join([ str(x) for x in dports ])
                    suffix.extend(('-m', 'multiport', '--dports={}'.format(dports)))
                suffix.extend(('-j', 'DNAT', '--to', s['to']))
                if 'source' in s:
                    addresses=s['source']
                    if not isinstance(addresses, list):
                        addresses=[ str(addresses) ]
                    for adresse in addresses:
                        cmd=prefix.copy()
                        cmd.extend(('-s', adresse))
                        cmd.extend(suffix)
                        cmds.append((cmd, _wait.yes, _ignore_error.no))
                else:
                    cmd=prefix.copy()
                    cmd.extend(suffix)
                    cmds.append((cmd, _wait.yes, _ignore_error.no))

        if "snat" in configuration_routes:
            snat=configuration_routes["snat"]
            if isinstance(snat, dict):
                snat=[snat]
            for s in snat:
                validate_keys(('out', 'protocole', 'dports', 'to-source' ,'source'), s)
                prefix=['iptables', '-t', 'nat', '-A', 'POSTROUTING']
                suffix=[]
                if 'out' in s:
                    _out=s['out']
                    if isinstance(_out, list):
                        _out=','.join(_out)
                    suffix.extend(('-o', _out))
                if 'protocole' in s: suffix.extend(('-p', s['protocole']))
                if 'dports' in s:
                    dports=s['dports']
                    if isinstance(dports, list):
                        dports= ','.join([ str(x) for x in dports ])
                    suffix.extend(('-m', 'multiport', '--dports={}'.format(dports)))
                suffix.extend(('-j', 'SNAT', '--to-source', s['to-source']))
                if 'source' in s:
                    addresses=s['source']
                    if not isinstance(addresses, list):
                        addresses=[ str(addresses) ]
                    for adresse in addresses:
                        cmd=prefix.copy()
                        cmd.extend(('-s', adresse))
                        cmd.extend(suffix)
                        cmds.append((cmd, _wait.yes, _ignore_error.no))
                else:
                    cmd=prefix.copy()
                    cmd.extend(suffix)
                    cmds.append((cmd, _wait.yes, _ignore_error.no))


        if "gw" in configuration_routes:
            gw=configuration_routes["gw"]
            cmds.append(self.commands.deleteDefaultRoute())
            cmds.append(self.commands.setDefaultRoute(gw))

        if "forward" in configuration_routes:
            if configuration_routes["forward"]:
                cmds.append(self.commands.enableForwarding())
                cmds.append(self.commands.enableForwarding6())
                cmds.append(self.commands.enableRouteLocalHost())
            else:
                cmds.append(self.commands.disableForwarding())
                cmds.append(self.commands.disableForwarding6())
                cmds.append(self.commands.disableRouteLocalHost())

        return cmds

    def configureInterface(self,configuration, interface, netns=None):
        self.debug('AbstractSystem:configureInterface : {}'.format(interface))
        cmds=self.getCmdsToConfigureInterface(configuration, interface)
        for cmd, wait, ignore_error in cmds:
            self.execCommandonNetns(netns, cmd, wait, ignore_error)

    def configureRoutes(self,configuration, netns=None):
        self.debug('AbstractSystem:configureRoutes')
        cmds=self.getCmdsToConfigureRoutes(configuration)
        for cmd, wait, ignore_error in cmds:
            self.execCommandonNetns(netns, cmd, wait, ignore_error)

    def enableMasquerade(self, interface, from_source, netns=None):
        self.debug('AbstractSystem:enableMasquerade')
        if self.isNetIPv4(from_source):
            self.execCommandonNetns(netns, self.commands.enableMasquerade(interface, from_source))
        elif self.isNetIPv6(from_source):
            self.execCommandonNetns(netns, self.commands.enableMasquerade6(interface, from_source))
        else:
            raise Exception(f"Subnet is invalid : {from_source}")

    def cleanAll(self):
        self.killProcesses()
        self.deleteAllNetns()
        self.removeInterfaces()

