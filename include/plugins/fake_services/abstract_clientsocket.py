#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import threading
import socket

class AbstractClientSocket(threading.Thread):
    lockCollection=threading.Lock()
    client_collection=[]

    def __init__(self, conn, debug_mode=False):
        threading.Thread.__init__(self)
        self.connection=conn
        self.eventTerminated = threading.Event()
        self.lockStoppingSocket=threading.Lock()
        self.debug_mode=debug_mode
        self.peer=None

    def debug(self, msg):
        if self.debug_mode:
            print (msg)

    def getClientSocketInstances():
        AbstractClientSocket.lockCollection.acquire()
        ret=AbstractClientSocket.client_collection.copy()
        AbstractClientSocket.lockCollection.release()
        return ret

    def bannerHandler(self):
        """Reimplement in order to send banner."""
        pass

    def readyReadHandler(self, data):
        """Reimplement in order to read data."""
        pass

    def sendDatas(self, data):
        if self.eventTerminated.is_set(): return
        self.debug("AbstractClientSocket.sendDatas()")
        try:
            self.connection.send(data)
        except socket.error:
            pass

    def connectedHandler(self):
        """Reimplement me"""
        pass # nothing to do here

    def run(self):
        self.debug("Create new Thread: {}".format(self.getName()))
        AbstractClientSocket.lockCollection.acquire()
        AbstractClientSocket.client_collection.append(self)
        AbstractClientSocket.lockCollection.release()
        self.peer=self.connection.getpeername()

        self.connectedHandler()

        self.connection.settimeout(30)
        self.bannerHandler()

        while True:
            if self.eventTerminated.is_set(): break
            try:
                data= self.connection.recv(1024)
            except socket.error:
                break
            if not data: break
            self.readyReadHandler(data)

        self.stop() # close socket properly
        AbstractClientSocket.lockCollection.acquire()
        AbstractClientSocket.client_collection.remove(self)
        AbstractClientSocket.lockCollection.release()

    def stop(self):
        """stop connection with client"""
        self.lockStoppingSocket.acquire()
        if self.eventTerminated.is_set():
            self.lockStoppingSocket.release()
            return # already stopped
        self.eventTerminated.set()
        self.lockStoppingSocket.release()
        self.debug ("stopping connection with client {0} - {1}".format(self.peer,self.getName()))
        try:
            self.connection.shutdown(socket.SHUT_RDWR)
            self.connection.close()
        except:
            pass # connection's broken ?
