#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

from wrapper_services import WrapperServices

class Pop3():
    banner=b"+OK Dovecot (Ubuntu) ready.\x0d\x0a"
    sig=b"-ERR Unknown command.\x0d\x0a-ERR Unknown command.\x0d\x0a"

if __name__ == '__main__':
    def signal_handler(signal, frame):
        service.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    service=WrapperServices(port=110, banner=Pop3.banner, sig=Pop3.sig, debug_mode=True)
    service.start()
    service.join()
