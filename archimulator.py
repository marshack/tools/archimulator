#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, argparse

from include.archi_parser import Architecture

__version__="1.1.0"

if __name__ == '__main__':
    import signal
    def signal_handler(signal, frame):
        arch.stop()

    # Parse Args
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', action='version', \
        help="show version", 
        version='%(prog)s version : {version}'.format(version=__version__))
    parser.add_argument("-f", "--file", metavar="FILENAME",dest="filename",
            help="architecture filename (yaml)", required=True)
    parser.add_argument("-s", "--substitute", metavar="SUB",dest="sub", nargs='+',
            help="dynamically replace strings in the configuration file. ex: -s '#{{ADDRESS}}#192.168.1.3#'", required=False)
    parser.add_argument("-m", "--monitor", metavar="SOCKFILENAME",dest="sockfilename",
            help="Monitor (CLI)")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-d", "--debug", help="Debug", action="store_true")
    group.add_argument("-q", "--quiet", help="Suppress messages to standard output", action="store_true")
    args=parser.parse_args()

    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    arch=Architecture(args.filename, __version__, args.sockfilename, args.sub, args.debug, args.quiet)
    arch.start()
    arch.join()
    sys.exit(arch.retcode)

