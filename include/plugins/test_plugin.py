#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


from plugins.abstract.abstract_plugin import AbstractPlugin

class TestPlugin(AbstractPlugin):
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractPlugin.__init__(self, current_instance, name, configuration, debug_enabled)

    def start(self):
        self.debug("TestPlugin:start")
        print("current machine name : {}".format(self.name))
        print("current machine configuration : {}".format(self.configuration))

    def stop(self):
        self.debug("TestPlugin:stop")


pluginName=TestPlugin
