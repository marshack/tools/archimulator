### kvm

> requiert : **qemu-kvm**

Ce plugin permet de démarrer une machine virtuelle jetable à partir d'une image, et de le raccorder à l’infrastructure.

Installation :
```bash
sudo apt install qemu-kvm ovmf
```

**Important :** 
 - Le plugin n'est pas en capacité de modifier la configuration réseau à l'intérieur de la VM (astuce : utiliser *cloud init*).
 - Un système de fichiers gérant la **copie légère** (CoW: *Copy on Write*) est requis. Par exemple **btrFS**.
 - Les fichiers disques (source et destination) devront être sur le **même système de fichiers** pour bénéficier de la **copie légère**.


* *image* : image de base de la machine virtuelle (requis - voir section *Fichier de configuration d'une image KVM*)
* *link* : interface réseau reliée au Switch (requis)

```yaml
    machines :
        machine-1 :
            kvm :
                image : alpine
                link : tap1
```

Ou (si plusieurs interfaces) :

```yaml
    machines :
        machine-1 :
            kvm :
                image : alpine
                interfaces:
                    - link : tap1
                      mac  : "00:02:00:00:00:01"
                    - link : tap2
                      mac  : "00:02:00:00:00:02"
```

#### Fichier de configuration d'une image KVM

Ce fichier décrit les caractéristiques d'une machine virtuelle. Exemple `alpine.yaml` :
```yaml
kvm:
    name : alpine
    version : 1.0
    configuration:
        keyboard : fr
        # guest_type :
        #  - 0 : Moteur KVM avec BIOS (par défaut, convient dans la plupart des cas)
        #  - 1 : Moteur KVM pour une machine invitée Windows (HyperV activé)
        #  - 2 : Moteur KVM avec UEFI
        guest_type : 0
        # fichier disque source
        source : /serve/origin/alpine.qcow2
        # fichier au format ISO 9660 (cdrom)
        iso : /serve/ISO/cloud-init.iso
        # mémoire, en Mo
        ram : 720
        # nombre de CPU
        cpus : 1
        # activer ou non les pilotes Virtio pour le réseau
        net_virtio : yes
        # scsi_virtio est déconseillé avec le système de fichiers btrFS
        scsi_virtio : no
        # compte système à utiliser pour le processus kvm/qemu
        account_kvm : vmunpriv
```

Les champs requis sont : *source* et *ram*

Si l'on souhaite créer un compte système dédié (*runas* au profit des VM) :
```bash
sudo groupadd vmunpriv
sudo useradd -rNM vmunpriv -G kvm -g vmunpriv -d /dev/null -s /sbin/nologin -c "Unprivileged virtual machine account"
sudo passwd -l vmunpriv
``` 

> **Important** :
> - À ce jour, l'interface n'est pas figée.
> - Les pilotes *virtio* permettent d'accélérer les accès disques, réseaux, ... entre la machine virtuelle et la machine hôte.
> - Le système de fichiers *btrFS* est toujours en cours de développement, et il est **fortement déconseillé** de l'utiliser avec le pilote *virtio*. Source : https://www.linux-kvm.org/page/Tuning_KVM

#### VNC

Il est possible d'activer VNC :
```yaml
machines :
    machine-1 :
        kvm :
            image : alpine
            link : tap1
            vnc:
                listen : 127.0.0.1
                port: 1
                password : "my_secret"
```

Dans ce cas, il faudra créer des règles de routage, afin de pouvoir le joindre de l'extérieur :
```yaml
routes :
        forward: yes
        nat:
            source: [ 192.168.5.0/24 ]
            out: vlan1
        dnat:
          - in: vlan1
            protocole : tcp
            dports: [ 5901 ]
            to : 127.0.0.1
```

#### Fichier de configuration du plugin

Le plugin KVM possède un fichier de configuration, *kvm_plugin.conf* :
```ini
[PATH]
# répertoire des fichiers de configuration des images
configurations=examples/kvm/
# Chemin vers un dossier où seront copiées les fichiers images disques qcow2 (requiert un système de fichiers ayant la capacité CoW, tel que BtrFS)
target_disk=/serve/tmp
# Dossier temporaire (sockets unix, ...)
tmp_dir=/tmp/archimulator/kvm/sock
```
