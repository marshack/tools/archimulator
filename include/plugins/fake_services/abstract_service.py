#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, threading
import socket, select

from abstract_clientsocket import AbstractClientSocket

class AbstractService(threading.Thread):
    def __init__(self, host, port, debug_mode=False):
        threading.Thread.__init__(self)
        self.eventTerminated = threading.Event()
        self.lockStopping=threading.Lock()
        self.host=host
        self.port=port
        self.debug_mode=debug_mode

    def debug(self, msg):
        if self.debug_mode:
            print (msg)

    def fatal(self, error_msg):
        sys.stderr.write ("{}\n".format(error_msg))
        sys.exit(1)

    def createSocketHandler(self, sock):
        """Reimplement to customize. Return a AbstractClientSocket object"""
        return AbstractClientSocket(sock, self.debug_mode)

    def run(self):
        self.debug("[*] Service listen on {}:{} ".format(self.host, self.port))
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((self.host, self.port))
        self.server.listen(5)
        while True:
            if self.eventTerminated.is_set(): break
            r, w, x = select.select([self.server], [], [], 0.8)
            if self.server in r:
                connection, address = self.server.accept()
                self.debug("[*] New connection from {}:{}".format(address[0], address[1]))
                connection.settimeout(30)
                try:
                    client=self.createSocketHandler(connection)
                    client.start()
                except:
                    sys.stderr.write ("[-] Client socket error: {0}, port {1}<->{2}\n".format(address[0], address[1], self.port))
                    continue


    def stop(self):
        self.lockStopping.acquire()
        if self.eventTerminated.is_set():
            self.lockStopping.release()
            return # already stopped

        self.eventTerminated.set()
        self.debug("[*] Shutting down...")
        for instance in AbstractClientSocket.getClientSocketInstances():
            try:
                instance.stop()
            except:
                pass

        try:
            self.server.close()
        except:
            pass

        self.debug("[*] Done")
        self.lockStopping.release()

if __name__ == '__main__':
    def signal_handler(signal, frame):
        abs_service.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)
    
    abs_service=AbstractService('0.0.0.0', 2222, True)
    abs_service.start()
    abs_service.join()
