### lxc

> requiert : **lxc**

Ce plugin permet de démarrer un container jetable à partir d'une image, et de le raccorder à notre architecture.

Installation :
```bash
sudo apt install lxc python3-lxc
```

> Un système de fichiers de type **btrfs** est requis (prend en charge CoW : *Copy on write*), ce qui permet d’accélérer la copie du système de fichiers lors de la création du container, et préserver l'espace de stockage.

Il est également possible de démarrer un programme ou un script dans le container.

* *image* : image de base du container (requis)
* *link* : interface réseau reliée au Switch (requis)
* *intname* :  nom de l'interface réseau dans le container (recommandé - généralement eth0, eth1, ...)
* *addresses* : liste d'adresses IP (optionnel)
* *gw* : passerelle par défaut (optionnel)
* *push* : copier un fichier, de la machine hôte vers le container (optionnel)
* *start* : programme lancé au démarrage, dans le container (optionnel). Ne doit pas être bloquant
* *command* : remplace la commande par défaut (optionnel)
* *quotas* : permet de limiter les ressources (optionnel)
* ...

```yaml
    machine2 :
        lxc :
            image : apache2
            interfaces:
                link : tap2
                intname : eth0
                addresses : [ 192.168.100.4/24 ]
                gw : 192.168.100.1
            push :
             - /etc/issue /etc/
             - /etc/aliases /etc/
            start : [ "/bin/echo", "start" ]
            command: [ "sleep", "infinity" ]
```

#### Quotas (optionnel)

Si l'on souhaite limiter les ressources pour un container lxd (exemple) :
```yaml
            quotas:
                cpus : 1
                disk: 300MB
                memory: 800MB
                memory-swap: 1GB
```

Activer le *re-mapping* utilisateurs, en ajoutant dans les fichiers `/etc/subuid` et `/etc/subgid` :
```
root:100000:65536
```

#### Fichier de configuration du plugin

Le plugin LXC possède un fichier de configuration, *lxc_plugin.conf* :
```ini
[PATH]
# Dossier contenant les images (système de fichiers btrfs)
source_dir=/serve/lxc/images
# dossier où seront stockées les containers jetables (doit être dans le même système de fichiers que les images)
tmp_dir=/serve/lxc/containers
```
