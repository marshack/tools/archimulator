#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


from plugins.abstract.abstract_netns_plugin import AbstractNetnsPlugin

class NetnsPlugin(AbstractNetnsPlugin):
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractNetnsPlugin.__init__(self, current_instance, name, configuration, debug_enabled)

    def start(self):
        self.debug("NetnsPlugin:start")
        AbstractNetnsPlugin.start(self)

    def stop(self):
        self.debug("NetnsPlugin:stop")
        AbstractNetnsPlugin.stop(self)

pluginName=NetnsPlugin
