#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# This is a Meta Services

from abstract_meta import AbstractMeta
from wrapper_services import WrapperServices

from imap import Imap
from pop3 import Pop3
from smtp import Smtp

if __name__ == '__main__':
    def signal_handler(signal, frame):
        meta.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    meta=AbstractMeta()
    meta.add(WrapperServices(port=143, banner=Imap.banner, sig=Imap.sig, debug_mode=True))
    meta.add(WrapperServices(port=993, with_ssl=True, banner=Imap.banner, sig=Imap.sig, debug_mode=True))
    meta.add(WrapperServices(port=110, banner=Pop3.banner, sig=Pop3.sig, debug_mode=True))
    meta.add(WrapperServices(port=995, with_ssl=True, banner=Pop3.banner, sig=Pop3.sig, debug_mode=True))
    meta.add(WrapperServices(port=25, banner=Smtp.banner, sig=Smtp.sig, debug_mode=True))
    meta.add(WrapperServices(port=465, with_ssl=True, banner=Smtp.banner, sig=Smtp.sig, debug_mode=True))

    # start
    meta.start()
    # wait before quit
    meta.join()
