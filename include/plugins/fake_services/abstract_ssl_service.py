#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os
import socket, ssl
import threading
import select
import pathlib

from abstract_clientsocket import AbstractClientSocket

class AbstractSSLservice(threading.Thread):
    def __init__(self, certificate="pem/server.pem", privatekey="pem/privkey.pem", \
        ssl_password="", host='0.0.0.0', port=8000, debug_mode=False):
        threading.Thread.__init__(self)
        self.certificate=self.getAbsolutePathFromFilename(certificate)
        self.privatekey=self.getAbsolutePathFromFilename(privatekey)
        self.ssl_password=ssl_password
        self.host=host
        self.port=port
        self.eventTerminated = threading.Event()
        self.lockStopping=threading.Lock()
        self.bindsocket=0
        self.debug_mode=debug_mode


    def debug(self, msg):
        if self.debug_mode:
            print (msg)

    def fatal(self, error_msg):
        sys.stderr.write ("{}\n".format(error_msg))
        sys.exit(1)

    def getAbsolutePathFromFilename(self, filename):
        if not os.path.exists(filename):
            script_absolute_path=pathlib.Path(__file__).parent.absolute().as_posix()
            filename="{}/{}".format(script_absolute_path, filename)
        return pathlib.Path(filename).absolute().as_posix()

    def createSocketHandler(self, sock):
        """Reimplement to customize. Return a AbstractClientSocket object"""
        return AbstractClientSocket(sock, self.debug_mode)

    def run(self):
        """start server"""
        try:
            context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        except:
            self.fatal("Impossible to create a new TLS/SSL context.")

        try:
            context.load_cert_chain (certfile=self.certificate,keyfile=self.privatekey, password=self.ssl_password)
        except IOError as file:
            self.fatal("Impossible to load certificate and/or private key: '{}'".format(file))

        self.bindsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bindsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.bindsocket.bind((self.host, self.port))
        except socket.error:
            self.fatal("Failed to bind to port {}".format(self.port))

        self.debug("[*] Service listen on {}:{} ".format(self.host, self.port))
        self.bindsocket.listen(5)

        while True:
            if self.eventTerminated.is_set(): break
            r, _, _ = select.select([self.bindsocket], [], [], 0.8)
            if self.bindsocket in r:
                connection, address = self.bindsocket.accept()
                self.debug("[*] New connection from {}:{}".format(address[0], address[1]))
                connection.settimeout(30)
                try:
                    ssl_sock = context.wrap_socket(connection, server_side=True)
                except ssl.SSLError as err:
                    sys.stderr.write ("SSL Error: {}\n".format(err))
                    continue

                try:
                    client=self.createSocketHandler(ssl_sock)
                    client.start()
                except:
                    sys.stderr.write ("Client socket error: {0}, port {1}<->{2}\n".format(address[0], address[1], self.port))
                    continue

    def stop(self):
        """stop server"""
        self.lockStopping.acquire()
        if self.eventTerminated.is_set():
            self.lockStopping.release()
            return # already stopped

        self.eventTerminated.set()
        self.debug("[*] Shutting down...")

        for instance in AbstractClientSocket.getClientSocketInstances():
            try:
                instance.stop()
            except:
                pass

        try:
            self.bindsocket.close()
        except:
            pass
        self.debug("[*] Done")

        self.lockStopping.release()


if __name__ == "__main__":
    def signal_handler(signal, frame):
        server.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    server=AbstractSSLservice("pem/server.pem", "pem/privkey.pem", host='0.0.0.0', port=8080, debug_mode=True)
    server.start()
    server.join()
