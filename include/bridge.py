#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

from include.abstract_system import AbstractSystem
from include.plugins.abstract.abstract_plugin import TapType, TypeInterface
from include.validate import validate_keys

class Bridge(AbstractSystem):
    def __init__(self, switch, virtual_interfaces, routes, current_instance, debug_enabled=False):
        AbstractSystem.__init__(self, debug_enabled)
        self.switch=switch
        self.virtual_interfaces=virtual_interfaces
        self.routes=routes
        self.current_instance=current_instance

    def start(self):
        self.debug("Bridge:start")
        validate_keys(('name', 'interfaces', 'stp'), self.switch)
        self.ns_bridge="{}-ns-br".format(self.current_instance)
        try:
            switch_name="bridge0"
            interfaces=self.switch["interfaces"]
            # create namespace for switch
            self.createNetnsIfnotexist(self.ns_bridge)
            # create the switch
            self.execCommandonNetns(self.ns_bridge, ('ip', 'link', 'add', 'name', switch_name, 'type', 'bridge', 'vlan_filtering', '1'))
            self.setUpInterface(switch_name, self.ns_bridge)
            # security (remove br interface from vid 1)
            self.removeVlanOnInterface(switch_name, "1", is_bridge=True, netns=self.ns_bridge)

            # enable iptables for bridge
            self.execCommandonNetns(self.ns_bridge, ('modprobe', 'br_netfilter'))
            self.execCommandonNetns(self.ns_bridge, ('sysctl', '-w', 'net.bridge.bridge-nf-call-iptables=0'))

            # Spanning Tree Protocol (optional)
            if "stp" in self.switch and self.switch["stp"]==True:
                self.execCommandonNetns(self.ns_bridge, ('ip', 'link', 'set', switch_name, 'type', 'bridge' ,'stp_state', '1'))

            self.debug('Bridge:createBridges - Configure Interfaces')
            for interface in interfaces.values():
                validate_keys(('vlan', 'tagged_vlans', 'link', 'taptype', 'intname', 'type'), interface)
                type_int=TypeInterface.internal
                link_is_bridge=False
                taptype=TapType.veth # default
                if 'taptype' in interface:
                    taptype=interface['taptype']

                if "type" in interface:
                    if interface["type"]=="external":
                        type_int=TypeInterface.external
                    elif interface["type"]=="internal":
                        pass # default
                    else:
                        raise Exception("Unknown type of interface : {}".format(interface["type"]))

                if type_int==TypeInterface.external:
                    if self.isBridgeInterface(interface["link"]): link_is_bridge=True
                    else: raise Exception("It's not possible to connect to an external interface that is not a bridge")

                if type_int==TypeInterface.internal or link_is_bridge:
                    if taptype==TapType.veth:
                        link_br="{}-A-{}".format(interface["link"], self.current_instance)
                        link_tap="{}-B-{}".format(interface["link"], self.current_instance)
                        # create the veth pair
                        self.execCommand(('ip', 'link', 'add', link_tap, 'type', 'veth', 'peer', 'name', link_br))
                    elif taptype==TapType.tuntap:
                        link_br="{}-{}".format(interface["link"], self.current_instance)
                        link_tap=link_br
                        self.execCommand(('ip', 'tuntap', 'add', link_br, 'mode', 'tap'))
                    self.interfaces_collection.append(link_tap)

                    # bring up the link
                    self.setUpInterface(link_tap)

                if link_is_bridge:
                    # attach tap interface on switch
                    self.execCommand(('ip', 'link', 'set', 'dev', link_tap, 'master', interface["link"]))

                vlan=None
                tagged_vlans=[]
                if "vlan" in interface:
                    vlan=str(interface["vlan"])
                elif "tagged_vlans" in interface:
                    tagged_vlans=interface["tagged_vlans"]
                else:
                    vlan="1"

                if not isinstance(tagged_vlans, list):
                    raise Exception("No tagged vlans found (it's not a list)")


                # move the interfaces to the namespaces
                self.attachIntToNetns(link_br, self.ns_bridge)

                # rename interface
                new_name_interface=interface["link"]
                if "intname" in interface and taptype!=TapType.tuntap:
                    new_name_interface=interface["intname"]
                self.renameInterface(link_br, new_name_interface, self.ns_bridge)
                link_br=new_name_interface

                # bring up the link
                self.setUpInterface(link_br, self.ns_bridge)

                # attach on switch
                self.execCommandonNetns(self.ns_bridge, ('ip', 'link', 'set', 'dev', link_br, 'master', switch_name))
                # security (remove interface from vid 1)
                self.removeVlanOnInterface(link_br, "1", netns=self.ns_bridge)

                if vlan!=None:
                    self.setVlanOnInterface(link_br, str(vlan), netns=self.ns_bridge)
                for tagged_vlan in tagged_vlans:
                    self.setVlanOnInterface(link_br, str(tagged_vlan), is_tagged=True, netns=self.ns_bridge)

            if self.virtual_interfaces:
                self.debug('Bridge:createBridges - Configure SVI')
                if not isinstance(self.virtual_interfaces, dict):
                    raise Exception("No SVI configuration found")

                for vlan_id, value in self.virtual_interfaces.items():
                    vlan_name="{}-{}".format(self.current_instance, value["intname"])
                    # SVI
                    self.execCommandonNetns(self.ns_bridge, ('ip', 'link', 'add', 'link', switch_name, 'name', vlan_name, 'type', 'vlan', 'id', str(vlan_id)))
                    self.setVlanOnInterface(switch_name, str(vlan_id), is_tagged=True, is_bridge=True, netns=self.ns_bridge)
                    self.configureInterface(value, vlan_name, self.ns_bridge)

            routes={}
            if self.routes:
                routes=self.routes
            if not "forward" in routes:
                # forward by default
                routes["forward"]=True
            if routes:
                self.debug('Bridge:createBridges - Configure routes')
                self.configureRoutes(routes, self.ns_bridge)

        except KeyError as e:
            raise Exception("arg {} is required".format(e.args[0]))

    def stop(self):
        self.debug("Bridge:stop")
        AbstractSystem.cleanAll(self)


