### docker

> requiert : **docker**

Ce plugin permet de démarrer un container jetable à partir d'une image, et de le raccorder à l’infrastructure.

Installation :
```bash
sudo apt install docker.io python3-docker
```

> Il est recommandé d'utiliser un pool de stockage de type **btrfs**, ce qui permet d’accélérer la copie du système de fichiers lors de la création du container, et préserver l'espace de stockage (https://docs.docker.com/storage/storagedriver/btrfs-driver/).

Il est également possible de démarrer un programme ou un script dans le container.

* *image* : image de base du container (requis)
* *link* : interface réseau reliée au Switch (requis)
* *intname* :  nom de l'interface réseau dans le container (recommandé - généralement eth0, eth1, ...)
* *addresses* : liste d'adresses IP (optionnel)
* *gw* : passerelle par défaut (optionnel)
* *push* : copier un fichier, de la machine hôte vers le container (optionnel)
* *start* : programme lancé au démarrage, dans le container (optionnel). Ne doit pas être bloquant
* *command* : remplace la commande par défaut (optionnel)
* *environment* : positionner des variables d'environnement (optionnel)
* *user* : spécifier un utilisateur et un groupe dans docker (optionnel)
* *quotas* : permet de limiter les ressources (optionnel)
* ...

```yaml
    db :
        docker :
            image : mariadb
            interfaces:
                link : tap2
                intname : eth0
                addresses : [ 192.168.100.4/24 ]
                gw : 192.168.100.1
            push :
             - /serve/db/hosts /etc/
            start : [ "/usr/bin/touch", "/tmp/debug" ]
            command: ["mysqld", "--character-set-server=utf8mb4", "--log-warnings=0" ]
            environment:
             - MYSQL_USER=root
             - MYSQL_PASSWORD=password
```

#### Utilisateur/Groupe

Exemples :

```yaml
    user : root
```
```yaml
    user : root:1000
```

#### Quotas (optionnel)

Si l'on souhaite limiter les ressources pour un container docker (exemple) :
```yaml
            quotas:
                cpus: .5
                cpuset-cpus: 0-1
                disk: 300MB
                memory: 800MB
                memory-swap: 1GB
```

##### Vérifications (une fois l'architecture démarrée) :

```bash
docker stats
```

**Note :** Pour plus de sécurité, il est recommandé d'activer l'espace de noms utilisateur sous Docker : https://docs.docker.com/engine/security/userns-remap/

Le fichier de configuration conseillé, pour utiliser *Archimulator* avec *Docker* (*/etc/docker/daemon.json*) :
```json
{
  "storage-driver": "btrfs",
  "userns-remap": "default",
  "bridge": "none",
  "iptables": false
}
```
et activation (**Toutes les images et containers existants seront perdus**) :
```bash
sudo systemctl restart docker.socket docker.service
```
