#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

from wrapper_services import WrapperServices

class Imap():
    banner=None
    sig=b"* OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE STARTTLS AUTH=PLAIN] Dovecot (Ubuntu) ready.\x0d\x0a"

if __name__ == '__main__':
    def signal_handler(signal, frame):
        service.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    service=WrapperServices(port=143, banner=Imap.banner, sig=Imap.sig, debug_mode=True)
    service.start()
    service.join()
