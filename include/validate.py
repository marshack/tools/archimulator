#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

def validate_keys(valid_keys, tree):
    if tree:
        for k in tree.keys():
            if k not in valid_keys:
                print("Warning : Unknown key '{}'".format(k))

if __name__ == '__main__':
    tree={'name': 'switch-demo', 'interfaces': {1: {'link': 'br0', 'intname': 'eth0', 'type': 'external', 'vlan': 100}, 2: {'link': 'tap1', 'vlan': 200, 'taptype': 0}}}
    valid_keys=('name', 'interfaces')
    validate_keys(valid_keys, tree)
