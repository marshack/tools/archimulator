#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os
import pathlib, importlib

sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), 'plugins'))

class PluginsManager():
    def __init__(self, current_instance, debug_enabled=False):
        self.debug_enabled=debug_enabled
        self.current_instance=current_instance
        self.plugins_collection=[]

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def printError(self, msg):
        sys.stderr.write("ERROR - {}\n".format(msg))

    def _plugin_class_from_name(self, plugin_name):
        plugins_dir = os.path.join(os.path.dirname(__file__), 'plugins')
        plugin_file="{}/{}_plugin.py".format(plugins_dir, plugin_name)
        if not os.path.isfile(plugin_file):
            raise Exception("Plugin {} doesn't exist : (file '{}' missing)".format(plugin_name, plugin_file))
        try:
            module_name = pathlib.Path(plugin_file).stem
            module = importlib.import_module(module_name)
            return module.pluginName
        except Exception as e:
            raise Exception("Impossible to load plugin {} : {}".format(plugin_name, e.args[0]))
        return None

    def loadPlugin(self, plugin_name, machine_name, configuration):
        self.debug("Plugins:loadPlugin {}".format(plugin_name))
        plugin_class=self._plugin_class_from_name(plugin_name)
        if plugin_class:
            plugin=plugin_class(self.current_instance, machine_name, configuration, self.debug_enabled)
            self.plugins_collection.append(plugin)
            plugin.start()

    def stopAll(self):
        self.debug("Plugins:stopAll")
        for p in self.plugins_collection:
            p.stop()

    def restartAll(self):
        self.debug("Plugins:restartAll")
        for p in self.plugins_collection:
            p.restart()

    def getTypeInterfaceFromPluginName(self, plugin_name):
        self.debug("Plugins:getTypeInterfaceFromPluginName {}".format(plugin_name))
        plugin_class=self._plugin_class_from_name(plugin_name)
        if plugin_class:
            return plugin_class.taptype
        return None

if __name__ == '__main__':
    plugins=PluginsManager(True)
    plugins.loadPlugin("test", "machine1", {'link': 'tap1', 'addresses': ['192.168.1.2/24'], 'gw': '192.168.1.1'})
    print(plugins.getTypeInterfaceFromPluginName('test'))
    plugins.stopAll()
