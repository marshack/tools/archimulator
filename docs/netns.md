### netns

C'est le module dont l'empreinte mémoire est la plus faible. Celui-ci créé un simple espace de nom réseau. Idéal pour recevoir des réponses ICMP (*echo reply*,  *destination port unreachable*, ...), faire office de routeur, etc ...

* *link* : interface réseau (requis)
* *addresses* : liste d'adresses IP (optionnel)
* *gw* : passerelle par défaut (optionnel)
* *routes* : voir § "routes" (optionnel)
* ...

Exemple (écriture simplifiée pour convenance) :

```yaml
    machine-1 :
        netns :
          link : tap1
          addresses : [ 192.168.1.2/24 ]
          gw : 192.168.1.1
```

Si on souhaite décrire plusieurs interfaces, avec par exemple des routes, il faudra utiliser la syntaxe complète :

```yaml
    machine-1 :
        netns :
            interfaces:
                - link : tap1
                  addresses : [ 192.168.1.2/24 ]
                - link : tap2
                  intname : eth2
            routes:
                gw : 192.168.1.1
                static:
                    - to : 192.168.2.0/24
                      dev: tap1
                      metric: 500
                forward: yes
```
