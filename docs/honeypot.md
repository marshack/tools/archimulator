### honeypot

Créé un simple espace de nom réseau, et démarre des programmes pour simuler des services.

* *link* : interface réseau (requis)
* *services* : liste des services réseaux à démarrer sur cet espace de nom (requis)
* *addresses* : liste d'adresses IP (optionnel)
* *gw* : passerelle par défaut (optionnel)
* ...

Exemple :

```yaml
    machine-1 :
        honeypot :
            interfaces:
                link : tap1
                addresses : [ 192.168.100.2/24 ]
                gw : 192.168.100.1
            services : [ssh, ftp, http, telnet]
```

Les services qu'il est possible de simuler, se trouvent dans le dossier `fake_services`. Vous pouvez créer votre propre simulateur de service, en créant votre programme et en le rajoutant dans ce dossier.

#### Meta-services

Les *meta-services* permettent de grouper les services par sujet (par commodité). Par exemple le meta-service **meta-windows** expose les services suivants : **msrpc**, **microsoft-ds**, **netbios-ssn**, ...

Les meta-services sont préfixés par : `meta-`.

Exemple :

```yaml
services : [meta-web]
```

est équivalent à :

```yaml
services : [http, https]
```

Liste des *meta-services* disponibles :

- *meta-web* (http, https)
- *meta-mx* (pop3, imap, smtp, pop3s, ...)
- *meta-ftp* (ftp, ftps)
- *meta-windows* (msrpc, netbios_ssn, microsoft_ds, ...)
