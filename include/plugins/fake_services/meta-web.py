#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# This is a Meta Services

from abstract_meta import AbstractMeta
from wrapper_services import WrapperServices

from http import Http

if __name__ == '__main__':
    def signal_handler(signal, frame):
        meta.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    meta=AbstractMeta()
    meta.add(WrapperServices(port=80, banner=Http.banner, sig=Http.sig, debug_mode=True))
    meta.add(WrapperServices(port=443, with_ssl=True, banner=Http.banner, sig=Http.sig, debug_mode=True))

    # start
    meta.start()
    # wait before quit
    meta.join()
