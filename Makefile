all: build

clean:
	find . -name \*.pyc -type f -delete
	find . -name __pycache__ -type d -delete

build: build_python

build_python:
	find -type f -name "*.py" -exec dirname {} \; 2>/dev/null | sort -u | xargs -r -I {} sh -c "python3 -m py_compile {}/*.py"

install: install_configuration install_program

install_program: build_python
	mkdir -p /opt/archimulator
	cp -Pr * /opt/archimulator/
	chown root:root /opt/archimulator -R
# to migrate 1.0.0 -> 1.0.1 , TODO remove this line in the future
	test -f /usr/local/bin/archimulator && rm -f /usr/local/bin/archimulator || /bin/true
	test -L /usr/local/bin/archimulator || ln -s /opt/archimulator/archimulator.py /usr/local/bin/archimulator

install_configuration:
	mkdir -p /etc/archimulator
	cp -n include/plugins/kvm/kvm_plugin.conf /etc/archimulator
	cp -n include/plugins/lxc/lxc_plugin.conf /etc/archimulator
	chmod o= /etc/archimulator -R

uninstall:
	rm -rf /opt/archimulator
	rm -rf /etc/archimulator
	rm -f /usr/local/bin/archimulator
