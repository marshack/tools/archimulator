#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

from wrapper_services import WrapperServices

class Http():
    banner=None
    sig=b"""HTTP/1.1 301 Moved Permanently\r
Server: nginx/1.10.3\r
Date: Tue, 28 Apr 2020 13:28:40 GMT\r
Content-type: text/html; charset=UTF-8\r
Content-Length: 185\r
\r\n\r\n<html>\r\n<head><title>301 Moved Permanently</title></head>\r\n<body bgcolor="white">\r\n<center><h1>301 Moved Permanently</h1></center>\r\n<hr><center>nginx/1.10.3</center>\r\n</body>\r\n</html>\r\n
"""

if __name__ == '__main__':
    def signal_handler(signal, frame):
        service.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    service=WrapperServices(port=80, banner=Http.banner, sig=Http.sig, debug_mode=True)
    service.start()
    service.join()
