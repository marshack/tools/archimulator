#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os, yaml
import threading, subprocess
import pathlib, uuid, binascii
import time
import socket

try:
    from plugins.kvm.schedule import Schedule
except:
    from schedule import Schedule

class KvmEngine(threading.Thread):
    last_mac_address=0x563412005452
    lock_mac_address=threading.Lock()
    def __init__(self, conf_filename, target_dir, tmp_dir="/tmp", tapnamelist=["tap0"], netns=None, vnc=None, vnc_password=None, debug_enabled=False):
        threading.Thread.__init__(self)
        self.conf_filename=self.getAbsolutePathFromFilename(conf_filename)
        self.debug_enabled=debug_enabled
        self.target_dir=target_dir
        self.tmp_dir=tmp_dir
        self.tmp_target_file=None
        self.tmp_sock_file=None
        self.tapnamelist=tapnamelist
        self.netns=netns
        self.proc_collection_lock=threading.Lock()
        self.proc_collection=[]
        self.schedule=Schedule()
        self.vnc=vnc
        self.vnc_password=vnc_password
        self.terminated=threading.Event()
        self.vmStarted=threading.Event()

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def printError(self, msg):
        sys.stderr.write("ERROR - {}\n".format(msg))

    def fatalError(self, msg=None):
        if msg:
            self.printError(msg)
        self.stop()

    def getAbsolutePathFromFilename(self, filename):
        if not os.path.exists(filename):
            script_absolute_path=pathlib.Path(__file__).parent.absolute().as_posix()
            filename="{}/{}".format(script_absolute_path, filename)
        return pathlib.Path(filename).absolute().as_posix()

    def getMacAdress(self):
        def int_to_bytes(value, size=None):
            if not size:
                size=(value.bit_length() + 7) // 8
            return value.to_bytes(size, 'big')

        KvmEngine.lock_mac_address.acquire()
        tmp_mac=int_to_bytes(KvmEngine.last_mac_address, 6)
        tmp_mac=binascii.hexlify(tmp_mac).decode()
        tmp_mac=':'.join(tmp_mac[i:i+2] for i in range(0, len(tmp_mac), 2))
        KvmEngine.last_mac_address+=1
        KvmEngine.lock_mac_address.release()
        return tmp_mac

    def execCommand(self, cmd, wait=True, ignore_errors=False):
        self.debug('KvmEngine:execCommand - {}'.format(" ".join(cmd)))
        stdout=[]
        p=subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.proc_collection_lock.acquire()
        self.proc_collection.append(p)
        self.proc_collection_lock.release()
        if not wait:
            return 0, []
        else:
            returncode=p.wait()
            stdout=p.stdout.read().decode()
            if returncode!=0:
                stderr=p.stderr.read().decode()
                if len(stderr)==0:
                    stderr=stdout # error message is in stdout
                stderr=stderr.replace('\n', ' ').strip()[0:80] # clean and limit size
                if not ignore_errors:
                    raise Exception(stderr)
                else:
                    self.debug("Warning - {}".format(stderr))
        self.proc_collection_lock.acquire()
        if p in self.proc_collection:
            self.proc_collection.remove(p)
        self.proc_collection_lock.release()
        return 0, stdout.split('\n')

    def killProcesses(self):
        if not self.proc_collection:
            return
        self.debug("KvmEngine:killProcesses")
        # terminate
        self.proc_collection_lock.acquire()
        for p in self.proc_collection:
            if p.returncode==None:
                self.debug("try to terminate pid {}".format(p.pid))
                p.terminate()
                try:
                    p.wait(0.4)
                except:
                    pass
        self.proc_collection_lock.release()
        time.sleep(0.5)
        # kill
        self.proc_collection_lock.acquire()
        for p in self.proc_collection:
            if p.returncode==None:
                self.debug("is alive, kill pid {}".format(p.pid))
                try:
                    p.kill()
                    p.wait(0.2)
                except:
                    pass
        self.proc_collection.clear()
        self.proc_collection_lock.release()

    def callback_singleShot(self):
        self.debug("KvmEngine:callback_singleShot")
        if self.vnc_password:
            self.setVncPassword(self.vnc_password)
            self.schedule.stop()

    def sendQemuCommand(self, cmd):
        if not self.vmStarted.is_set():
            return False
        self.debug("KvmEngine:sendQemuCommand cmd='{}'".format(cmd))
        if self.tmp_sock_file and os.path.exists(self.tmp_sock_file):
            try:
                client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                client.connect(self.tmp_sock_file)
                cmd+="\n"
                client.send(cmd.encode('utf-8'))
                time.sleep(0.2)
                client.close()
            except Exception as e:
                self.printError("An error occured when sending command (cmd='{}'): {}".format(cmd, e))
                return False
        else:
            self.printError("Couldn't Connect to {}".format(self.tmp_sock_file))
            return False
        return True

    def setVncPassword(self, password):
        self.debug("KvmEngine:setVncPassword")
        return self.sendQemuCommand("set_password vnc {}".format(password))

    def destroyVM(self):
        self.debug("KvmEngine:destroyVM")
        return self.sendQemuCommand("quit")

    def restartVM(self):
        self.debug("KvmEngine:restartVM")
        return self.sendQemuCommand("system_reset")

    def readYamlFile(self):
        self.debug('KvmEngine:readYamlFile')
        try:
            with open(self.conf_filename) as f:
                return yaml.safe_load(f)
        except OSError as e:
            self.printError("{} (filename : {})".format(e.strerror, self.conf_filename))
        except:
            self.printError("Impossible to parse yaml file {}".format(self.conf_filename))
        return None

    def generateUUID(self):
        return str(uuid.uuid4())

    def createDirIfNotExist(self, directory):
        if self.terminated.is_set():
            return
        if not os.path.isdir(directory):
            try:
                os.makedirs(directory, 0o750)
            except:
                self.fatalError("Impossible to create directory : {}".format(directory))

    def CopyOnRawFile(self, source, target):
        if self.terminated.is_set():
            return
        # CoW
        cmd=('cp', '--reflink=always',  source, target)
        try:
            self.execCommand(cmd)
        except:
            self.fatalError("Impossible to copy file : {}->{}".format(source, target))

    def cleanAll(self):
        self.debug("KvmEngine:cleanAll")
        if self.tmp_sock_file:
            try:
                os.remove(self.tmp_sock_file)
            except:
                pass
        if self.tmp_target_file:
            try:
                os.remove(self.tmp_target_file)
            except:
                pass
        self.tmp_target_file=None
        self.tmp_sock_file=None

    def startVM(self, name, keyboard, ram, cpus=1, guest_type=0, net_virtio=False, scsi_virtio=False, \
        account_kvm="root", iso_file=None):
        if self.terminated.is_set():
            return
        cmd=[]
        if os.path.exists("/dev/kvm"):
            cmd.append("kvm")
        else:
            cmd.append("qemu-system-x86_64")

        cmd.extend(("-runas", account_kvm))
        cmd.extend(("-k", keyboard, "-name", name))

        if guest_type==0:
            # default
            cmd.extend(("-boot", "c"))
        elif guest_type==1:
            # Windows with HyperV
            cmd.extend(("-cpu", "SandyBridge,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time,+vmx", "-boot", "c"))
        elif guest_type==2:
            cmd.extend(("-boot", "menu=off,order=c", "-bios", "/usr/share/ovmf/OVMF.fd"))
        else:
            self.fatalError("Bad 'guest_type' : {}".format(guest_type))

        # CPU
        cmd.extend(("-smp", "cores={},threads=1,sockets=1".format(cpus)))
        # no UI
        cmd.append("-nographic")

        if self.vnc:
            cmd.extend(("-vnc", self.vnc))

        # RAM
        cmd.extend(("-m", ram))
        # virtio balloon device allows KVM guests to reduce their memory size
        cmd.extend(("-device", "virtio-balloon"))

        # Use a USB tablet instead of the default PS/2 mouse. Recommend, because the tablet sends the mouse cursor's position to match the host mouse cursor
        cmd.extend(("-device", "ich9-usb-ehci1,id=usb,bus=pci.0,addr=0x5.0x7", \
            "-device", "ich9-usb-uhci1,masterbus=usb.0,firstport=0,bus=pci.0,multifunction=on,addr=0x5", \
            "-device", "usb-tablet,id=input0,bus=usb.0,port=1"))

        # network interface
        device="e1000" # default
        if net_virtio:
            device="virtio-net-pci"
        for tapname in self.tapnamelist:
            mac=""
            if isinstance(tapname, tuple):
                if len(tapname)>=2:
                    mac=tapname[1]
                tapname=tapname[0]
            if len(mac)==0:
                # get new hw address
                mac=self.getMacAdress()
            cmd.extend(("-netdev", "tap,id={0},ifname={0},script=no,downscript=no".format(tapname)))
            cmd.extend(("-device", "{},romfile=,netdev={},mac={}".format(device, tapname, mac)))

        filename_cmd="file={}".format(self.tmp_target_file)
        if scsi_virtio:
            filename_cmd="{},if=virtio,cache=off".format(filename_cmd)
        # hard disk
        cmd.extend(("-drive", filename_cmd))
        # qemu/kvm process have a monitor accessible via a UNIX socket
        cmd.extend(("-monitor", "unix:{},server,nowait".format(self.tmp_sock_file)))

        # iso
        if iso_file:
            if os.path.isfile(iso_file):
                cmd.extend(("-cdrom", iso_file))
            else:
                self.printError("File {} doesn't exist".format(iso_file))

        # execute in netns
        if self.netns:
            new_cmd=['ip', 'netns', 'exec', self.netns]
            new_cmd.extend(cmd)
            cmd=new_cmd

        try:
            self.vmStarted.set()
            self.execCommand(cmd)
            self.vmStarted.clear()
        except Exception as e:
            self.vmStarted.clear()
            self.fatalError(e.args[0])


    def stop(self):
        self.debug('KvmEngine:stop')
        if not self.terminated.is_set():
            self.terminated.set()
            self.schedule.stop()
            if self.vmStarted.is_set():
                self.destroyVM()
            self.killProcesses()
            self.cleanAll()

    def run(self):
        if not os.path.isfile(self.conf_filename):
            self.fatalError("yaml file doesn't exist : {}".format(self.conf_filename))
            return

        self.config_kvm_yaml=self.readYamlFile()
        if not self.config_kvm_yaml:
            self.fatalError()
            return

        try:
            config_kvm_yaml=self.config_kvm_yaml["kvm"]
            name=config_kvm_yaml["name"]
            configuration=config_kvm_yaml["configuration"]
            try:
                keyboard=configuration["keyboard"]
            except:
                keyboard="en-us"
            try:
                guest_type=configuration["guest_type"]
            except:
                guest_type=0
            qcow2_source_file=configuration["source"]
            ram=str(configuration["ram"])
            try:
                cpus=configuration["cpus"]
            except:
                cpus=1
            try:
                net_virtio=configuration["net_virtio"]
            except:
                net_virtio=False
            try:
                scsi_virtio=configuration["scsi_virtio"]
            except:
                scsi_virtio=False
            try:
                account_kvm=configuration["account_kvm"]
            except:
                account_kvm="root"
            try:
                iso_file=configuration["iso"]
            except:
                iso_file=None
        except KeyError as e:
            self.fatalError("arg {} is required (missing in yaml file)".format(e.args[0]))
            return

        if not os.path.exists(qcow2_source_file):
            self.fatalError("Source file doesn't exist : {}".format(qcow2_source_file))
            return

        self.createDirIfNotExist(self.tmp_dir)
        _uuid=self.generateUUID()
        self.tmp_target_file="{}/target_{}.qcow2".format(self.target_dir, _uuid)
        self.tmp_sock_file="{}/kvm_{}.sock".format(self.tmp_dir, _uuid)
        # CoW
        self.CopyOnRawFile(qcow2_source_file, self.tmp_target_file)
        if self.vnc_password and not self.terminated.is_set():
            self.schedule.singleShot(8, self.callback_singleShot)
            self.schedule.start()
        self.startVM(name, keyboard, ram, cpus, guest_type, net_virtio, scsi_virtio, account_kvm, iso_file)
        self.stop()

if __name__ == '__main__':
    import signal
    def signal_handler(signal, frame):
        kvm_engine.stop()

    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    kvm_engine=KvmEngine("../../../examples/kvm/alpine.yaml", "/serve/tmp/", "/tmp/sock_kvm_engine", [("tap0", "00:02:00:00:00:01")], \
        vnc='127.0.0.1:0,password=on', vnc_password="secret", debug_enabled=True)

    kvm_engine.start()
    kvm_engine.join()

