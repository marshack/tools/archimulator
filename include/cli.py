#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os, threading
import socket, select

class CliClientSocket(threading.Thread):
    lockCollection=threading.Lock()
    client_collection=[]

    def __init__(self, conn, version="unknown", callback_stop=None, callback_restart=None, debug_mode=False):
        threading.Thread.__init__(self)
        self.connection=conn
        self.eventTerminated = threading.Event()
        self.lockStoppingSocket=threading.Lock()
        self.callback_stop=callback_stop
        self.callback_restart=callback_restart
        self.debug_mode=debug_mode
        self.cli_version="0.9.0_dev"
        self.archimulator_version=version

    def debug(self, msg):
        if self.debug_mode:
            print (msg)

    def getClientSocketInstances():
        CliClientSocket.lockCollection.acquire()
        ret=CliClientSocket.client_collection.copy()
        CliClientSocket.lockCollection.release()
        return ret

    def sendPrompt(self):
        self.sendDatas("> ")

    def readyReadHandler(self, data):
            data = data.decode('utf-8').strip()
            for d in data.split('\n'):
                self.debug("CLI : receive command {}".format(d))
                if d=="help":
                    self.sendDatas("Commands :\n\thelp - help message\n\tquit - quit cli\n\tstop - stop instance\n\trestart - restart instance\n")
                elif d=="quit":
                    self.sendDatas("bye ...\n")
                    self.connection.close()
                    self.debug("CLI: Close current connection")
                    break
                elif d=="stop":
                    self.cmd_stop()
                elif d=="restart":
                    self.cmd_restart()
                else:
                    self.sendDatas("unknown command : {}\n".format(d))
                    self.debug("CLI : Unknown command {}".format(d))
                self.sendPrompt()

    def sendDatas(self, data):
        if self.eventTerminated.is_set(): return
        try:
            self.connection.send(data.encode('utf-8'))
        except socket.error:
            pass

    def connectedHandler(self):
        to_send=f"\nWelcome to the Archimulator Command-Line Interface (CLI)!\nArchimulator version : {self.archimulator_version}\nCLI version : {self.cli_version}\n"
        to_send+="\n'help' : Lists all commands available\n\n"
        self.sendDatas(to_send)

    def cmd_stop(self):
        self.debug("Cli:cmd_stop")
        if callable(self.callback_stop):
            self.callback_stop()

    def cmd_restart(self):
        self.debug("Cli:cmd_restart")
        if callable(self.callback_restart):
            self.callback_restart()

    def run(self):
        self.debug("Create new Thread: {}".format(self.getName()))
        CliClientSocket.lockCollection.acquire()
        CliClientSocket.client_collection.append(self)
        CliClientSocket.lockCollection.release()

        self.connectedHandler()
        self.sendPrompt()

        while True:
            if self.eventTerminated.is_set(): break
            try:
                data=self.connection.recv(1024)
            except socket.error:
                break
            if not data: break
            self.readyReadHandler(data)

        self.stop() # close socket properly
        CliClientSocket.lockCollection.acquire()
        CliClientSocket.client_collection.remove(self)
        CliClientSocket.lockCollection.release()

    def stop(self):
        """stop connection with client"""
        self.lockStoppingSocket.acquire()
        if self.eventTerminated.is_set():
            self.lockStoppingSocket.release()
            return # already stopped
        self.eventTerminated.set()
        self.lockStoppingSocket.release()
        self.debug ("stopping connection with client {0}".format(self.getName()))
        try:
            self.connection.shutdown(socket.SHUT_RDWR)
            self.connection.close()
        except:
            pass # connection's broken ?

class Cli(threading.Thread):
    def __init__(self, socket_file, version="unknown", callback_stop=None, callback_restart=None, debug_enabled=False):
        threading.Thread.__init__(self)
        self.eventTerminated = threading.Event()
        self.lockStopping=threading.Lock()
        self.socket_file=socket_file
        self.debug_enabled=debug_enabled
        self.callback_stop=callback_stop
        self.callback_restart=callback_restart
        self.connection=None
        self.archimulator_version=version

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def fix_right(self, _struct):
        self.debug("Cli:fix_right")
        import pwd
        import grp
        filename=_struct[0]
        user=_struct[1]
        try:
            uid = pwd.getpwnam(user).pw_uid
        except:
            sys.stderr.write("Warning - User {} doesn't exist\n".format(user))
            return
        if len(_struct)>2:
            group=_struct[2]
        else:
            sys.stderr.write("Warning - Group missing\n")
            return
        try:
            gid = grp.getgrnam(group).gr_gid
        except:
            sys.stderr.write("Warning - Group {} doesn't exist\n".format(group))
            return

        try:
            os.chown(filename, uid, gid)
        except:
            sys.stderr.write("Warning - Impossible to change owner\n")
        if len(_struct)>3:
            mod=_struct[3]
            try:
                os.chmod(filename, int(mod, base=8))
            except:
                sys.stderr.write("Warning - Impossible to chmod : {}\n".format(mod))

    def createSocketHandler(self, sock):
        return CliClientSocket(sock, self.archimulator_version, \
                callback_stop=self.callback_stop, \
                callback_restart=self.callback_restart, \
                debug_mode=self.debug_enabled)

    def run(self):
        tmp_struct=self.socket_file.split(':')
        self.socket_file=tmp_struct[0]
        if os.path.exists(self.socket_file):
            os.remove(self.socket_file)

        self.debug("CLI : Opening socket : {}".format(self.socket_file))
        self.server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

        try:
            self.server.bind(self.socket_file)
        except:
            sys.stderr.write("Warning - Impossible to bind : {}\n".format(self.socket_file))
            return

        self.debug("CLI : Listening...")
        try:
            self.server.listen(5)
        except:
            sys.stderr.write("Warning - Impossible to listen : {}\n".format(self.socket_file))
            return

        if len(tmp_struct)>1:
            self.fix_right(tmp_struct)

        while True:
            if self.eventTerminated.is_set(): break
            r, _, _ = select.select([self.server], [], [], 0.8)
            if self.server in r and not self.eventTerminated.is_set():
                try:
                    connection, _ = self.server.accept()
                except:
                    sys.stderr.write ("Client connection failed")
                    continue
                self.debug("CLI : New connection")
                try:
                    client=self.createSocketHandler(connection)
                    client.start()
                except:
                    sys.stderr.write ("Client socket error")
                    continue

    def stop(self):
        self.lockStopping.acquire()
        if self.eventTerminated.is_set():
            self.lockStopping.release()
            return # already stopped

        self.eventTerminated.set()
        self.debug("CLI: Shutting down...")
        for instance in CliClientSocket.getClientSocketInstances():
            try:
                instance.stop()
            except:
                pass

        try:
            self.server.close()
        except:
            pass

        try:
            os.remove(self.socket_file)
        except:
            pass
        self.connection=None
        self.debug("CLI: Done")
        self.lockStopping.release()

if __name__ == '__main__':
    def signal_handler(signal, frame):
        print("Abort ...")
        cli.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    # cli=Cli("/tmp/socketfile.sock")
    # or (force owner/group/mod) :
    cli=Cli("/tmp/socketfile.sock:utilisateur:utilisateur:660", debug_enabled=True)
    cli.start()
    cli.join()
