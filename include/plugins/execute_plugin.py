#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


import subprocess

from plugins.abstract.abstract_netns_plugin import AbstractNetnsPlugin
from include.validate import validate_keys

class ExecutePlugin(AbstractNetnsPlugin):
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractNetnsPlugin.__init__(self, current_instance, name, configuration, debug_enabled)
        self.stop_cmd=None
        self.internal_keys.extend(('start' , 'stop'))

    def exec(self, cmd):
        new_cmd=['ip', 'netns', 'exec', self.netns_name ]
        new_cmd.extend(cmd)
        self.debug('ExecutePlugin:exec - {}'.format(" ".join(new_cmd)))
        p=subprocess.Popen(new_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()

    def start(self):
        self.debug("ExecutePlugin:start")
        AbstractNetnsPlugin.start(self)

        start_cmd=self.configuration["start"]
        if not isinstance(start_cmd, list):
            raise Exception("No start command found (it's not a list)")

        self.exec(start_cmd)

        if "stop" in self.configuration:
            self.stop_cmd=self.configuration["stop"]
            if not isinstance(self.stop_cmd, list):
                raise Exception("No stop command found (it's not a list)")

    def stop(self):
        self.debug("ExecutePlugin:stop")
        if self.stop_cmd:
            self.exec(self.stop_cmd)

        AbstractNetnsPlugin.stop(self)

pluginName=ExecutePlugin
