#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# This is a Meta Services

from abstract_meta import AbstractMeta
from wrapper_services import WrapperServices

from microsoft_ds import MicrosoftDs
from msrpc import MSrpc
from netbios_ssn import NetbiosSSN
from ms_http_api import MsHttpAPI

if __name__ == '__main__':
    def signal_handler(signal, frame):
        meta.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    meta=AbstractMeta()
    meta.add(WrapperServices(port=135, banner=MSrpc.banner, sig=MSrpc.sig, debug_mode=True))
    meta.add(WrapperServices(port=139, banner=NetbiosSSN.banner, sig=NetbiosSSN.sig, debug_mode=True))
    meta.add(WrapperServices(port=445, banner=MicrosoftDs.banner, sig=MicrosoftDs.sig, debug_mode=True))
    meta.add(WrapperServices(port=5357, banner=MsHttpAPI.banner, sig=MsHttpAPI.sig, debug_mode=True))

    # start
    meta.start()
    # wait before quit
    meta.join()
