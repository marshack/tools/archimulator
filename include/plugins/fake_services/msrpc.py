#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

from wrapper_services import WrapperServices

class MSrpc():
    banner=b"\x05\x00\x0d\x03\x10\x00\x00\x00\x18\x00\x00\x00\x00\x08\x01\x40\x04\x00\x01\x05\x00\x00\x00\x00"
    sig=None

if __name__ == '__main__':
    def signal_handler(signal, frame):
        service.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    service=WrapperServices(port=135, banner=MSrpc.banner, sig=MSrpc.sig, debug_mode=True)
    service.start()
    service.join()
