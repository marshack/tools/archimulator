_This program is licensed to you under the terms of the GNU General Public License version 3.0 or later_

# Présentation

Ce programme permet de simuler un environnement réseau et système (machines, vlans, routage, services, ...).

Il peut y avoir plusieurs instances d'une même architecture. En effet, chaque élément (machine, *switch*, routeur) s’exécute dans son propre espace de nom réseau.


# Prérequis

Les paquets *iproute2*, *python3*, *python3-yaml* et *udhcpc* sont requis.

Sous Debian/Ubuntu
```bash
sudo apt install iproute2 udhcpc python3 python3-yaml
```

# Usage

```bash
$ python3 archimulator.py -h
usage: archimulator.py [-h] [-v] -f FILENAME [-s SUB [SUB ...]]
                       [-m SOCKFILENAME] [-d | -q]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show version
  -f FILENAME, --file FILENAME
                        architecture filename (yaml)
  -s SUB [SUB ...], --substitute SUB [SUB ...]
                        dynamically replace strings in the configuration file.
                        ex: -s '#{{ADDRESS}}#192.168.1.3#'
  -m SOCKFILENAME, --monitor SOCKFILENAME
                        Monitor (CLI)
  -d, --debug           Debug
  -q, --quiet           Suppress messages to standard output
```

Exemple, pour instancier une architecture décrite dans le fichier `simple.yaml` : 

```bash
$ sudo python3 archimulator.py -f examples/simple.yaml
Creating ...
Creation complete after 1s
Instance : 084fd

```

Il faudra utiliser la combinaison de touches `CTRL+C` pour arrêter l'instance.

Une fois l'architecture en place, les espaces de noms réseau sont visualisables avec la commande `ip netns` :

```bash
$ ip netns
084fd-machine-1 (id: 1)
084fd-ns-br (id: 0)
```

Dans cet exemple, l'instance `084fd` a deux espaces de noms : un espace de nom pour *machine-1*, et un espace de nom pour le Switch/routeur (*ns-br*).

Si l'on souhaite se connecter dans un espace de noms réseau en particulier  :

```bash
sudo ip netns exec {name} {command}
```

Exemple :

```bash
sudo ip netns exec 084fd-machine-1 /bin/bash
```

Dans cet exemple, `exit` pour sortir de l'espace de nom réseau *084fd-machine-1*.

# Fichier de description d'une architecture


Il faut créer un fichier de type *yaml*, par architecture.

à minima, les clés `archi`, `switch`, et `machines` doivent êtres déclarées.

Exemple 1 :

```yaml
archi:
    name : simple
    version : 1.0
switch:
    name : switch-simple
    interfaces :
        1 :
            link : tap1
            vlan : 10
        2 :
            link : tap2
            tagged_vlans : [10]
vlans :
    10 :
        intname: vlan10
        addresses : [ 192.168.100.1/24 ]
machines :
    machine-1 :
        netns :
            link : tap1
            addresses : [ 192.168.100.2/24 ]
            gw : 192.168.100.1
```

Exemple 2, avec des services de type "pot de miel" :

```yaml
archi:
    name : honeypots
    version : 1.0
switch:
    name : switch-honeypots
    interfaces :
        1 :
            link : tap1
            vlan : 10
vlans :
    10 :
        intname: vlan10
        addresses : [ 192.168.100.1/24 ]
machines :
    machine-1 :
        honeypot :
            interfaces:
                link : tap1
                addresses : [ 192.168.100.2/24 ]
                gw : 192.168.100.1
            services : [ssh, ftp, telnet]
```

Exemple 3, en utilisant le plugin `lxc` , et en raccordant une interface `br0` de la machine hôte sur le Switch de notre architecture :

```yaml
archi:
    name : lxc
    version : 1.0
switch:
    name : switch-lxc
    interfaces :
        1 :
            link : tap1
            vlan : 1
        3 :
            link : br0
            type : external
            vlan : 1
machines :
    machine-1 :
        lxc :
            image : alpine
            interfaces:
                link : tap1
                intname : eth0
```

D'autres exemples sont disponibles dans le dossier *examples*.

# archi

Le mot clé "*archi*", permet de décrire sommairement notre architecture (au profit de l'utilisateur/mainteneur).

 * *name* : nom de l'architecture (requis)
 * *description* : brève description (optionnel)
 * *version* : version de ce fichier (optionnel)

# switch

Le mot clé "*switch*", permet de décrire le switch/commutateur (ports, vlans, type, ...)

 * *name* : nom du switch (optionnel)
 * *interfaces* : liste des interfaces du switch/commutateur (requis, voir *interfaces* ci-dessous)
 * *stp* : activer/désactiver le protocole STP (optionnel. Par défaut désactivé)

### Spanning Tree Protocol (STP)

Pour activer le protocole STP :

```yaml
switch:
    stp : true
    interfaces :
        1 :
            link : br0
            type : external
            vlan : 1
```

# Interfaces

Différents types d'interfaces sont disponibles :

* *internal* : Interface interne au Switch, et géré par l'application (par défaut)
* *external* : Interface externe (existante), et qui sera connectée au *switch* de notre architecture

Chaque interface peut avoir un nom plus "*user friendly*" en utilisant le mot clé : `intname`. C'est sous ce nom là que l'interface sera visible dans l'espace de noms réseau.

Exemple :

```yaml
    interfaces :
        1 :
            link : br0
            intname : eth0
            type : external
            vlan : 1
        2 :
            link : tap1
            intname : eth1
            vlan : 100
            # le type par defaut est internal si non renseigné
```

Le mot clé `link` permet de raccorder une interface d'une machine, à une interface du Switch virtuel, il faut le voir comme un "cordon de brassage virtuel". Par exemple, pour raccorder l'interface 1 du switch, à l'interface *eth0* de la *machine-1* :


```yaml
switch:
    interfaces :
        1 :
            link : tap6
machines :
    machine-1 :
        netns :
            link : tap6
            intname : eth0
```

### vlans

Sur une interface du switch, il est possible de "taguer" sur un ou plusieurs vlans (trames encapsulées sur du 802.1q).

```yaml
switch:
    interfaces :
        1 :
            link : tap1
            tagged_vlans : [10, 20, 30 ]
```

Ou bien d'affecter le port à un vlan particulier (non tagué) :

```yaml
switch:
    interfaces :
        1 :
            link : tap1
            vlan : 10
```

> Si pas de vlan de défini, le port sera rattaché au vlan par défaut (vlan 1)

## Adressage

On peut définir des adresses statiques :

* *addresses* : liste d'adresses IP
* *gw* : passerelle par défaut

Ou bien attribuer une adresse IP de façon dynamique :

* *dhcp4* : active DHCP client sur l'interface (IPv4)

* *accept-ra* : accepter/refuser les annonces de routeur qui permet au noyau de configurer automatiquement IPv6. Lorsqu'il est activé, accepte les annonces de routeur. Lorsqu'il est désactivé, ne répond pas aux annonces de routeur. S'il n'est pas défini, utilise le paramètre par défaut du noyau hôte.

> la clé *dhcp6* n'existe pas dans *archimulator*. Nous recommandons la configuration automatique sans état des adresses IPv6 à l'aide de la méthode **SLAAC** (*Stateless Automatic Auto Configuration*), en ajoutant `accept-ra : yes` sur l'interface désirée.

Exemple :
```yaml
    dhcp4: yes
    accept-ra: yes
    addresses : [ 192.168.100.2/24,  192.168.100.3/24 ]
    gw : 192.168.100.1
```

Il est également possible de forcer l'adresse physique d'une interface (MAC) :
```yaml
vlans :
    2 :
        intname: vlan2
        addresses : [ 192.168.5.1/24 ]
        mac  : "00:02:00:00:00:02"
machines :
    machine-1 :
        netns :
            interfaces:
                - link : tap2
                  addresses : [ 192.168.5.2/24 ]
                  gw : 192.168.5.1
                  mac  : "00:02:00:00:00:03"
```

## Switch Virtual Interface (SVI)

Pour autoriser le routage inter-vlan sur le commutateur, il est nécessaire de configurer des interfaces SVI. 
Les adresses IP de ces interfaces sont les **passerelles par défaut** des VLAN associés.

> Par défaut, le commutateur transfère les paquets d'une interface vers une autre (voir "Routes" ci-dessous)

```yaml
archi:
    name : SVI example
    version : 1.0
switch:
    name : switch
    interfaces :
        1 :
            link : tap1
            vlan : 10
        2 :
            link : tap2
            vlan : 10
        3 :
            link : tap3
            vlan : 11
        4 :
            link : tap4
            vlan : 12
vlans :
    10 :
        intname: vlan10
        addresses : [ 192.168.110.254/24 ]
    11 :
        intname: vlan11
        addresses : [ 192.168.111.254/24 ]
```

Dans cet exemple, le routage est possible entre *vlan10* et *vlan11*. *vlan12* se retrouve isolé.


## Routes

> Applicable sur une machine (sauf pour le plugin *kvm*), ou bien sur le commutateur virtuel (dans ce cas, écrire les règles à la racine de l'arbre yaml).

* *forward* : Activer/désactiver le transfert de paquets, d'une interface vers une autre. `forward : yes` est implicite sur le commutateur, si ce comportement n'est pas désiré sur ce dernier : `forward : no`

Exemple sur une machine :
```yaml
    machines:
        machine-1:
            netns:
                interfaces:
                    link : veth2
                routes:
                    forward: yes
```

Il est possible d'activer la translation d'adresses :

* *source* : Active NAT sur les adresses IP données
* *out* : Interface où sera appliquée le NAT

Exemple :
```yaml
    routes:
        nat:
            source: [ 192.168.100.0/24,  192.168.200.0/24]
            out: vlan1
```

Possibilité de définir des routes statiques :

```yaml
    routes:
        static:
            - to: 192.168.100.0/24
              via: 10.0.2.51
            - to: 192.168.200.0/24
              dev: veth2
              metric: 500
```

Destination NAT :

Exemple pour une DMZ :
```yaml
    routes:
        dnat:
            in: vlan1
            to : 192.168.5.2
```

Ou pour des adresses sources et ports définis :
```yaml
    routes:
        dnat:
          - in: vlan1
            protocole : tcp
            dports: [ 21, 22, 23, 80, 443, 990 ]
            to : 192.168.5.2
          - in: vlan1
            source: [ 172.24.8.9 ]
            protocole : tcp
            dports: [ 8080 ]
            to : 192.168.5.3:80
```

Source NAT :

Exemple pour substituer toutes les adresses sources arrivant sur une interface, par une adresse source spécifique.

```yaml
    routes:
        snat:
          - out : vlan200
            to-source : 192.168.200.1
```

Ou pour des adresses sources et ports définis :
```yaml
    routes:
        snat:
          - out : vlan200
            protocole : tcp
            dports: [ 80,443 ]
            source: [ 172.24.8.9 ]
            to-source : 192.168.200.1
```

# Plugins

## Machines

Différents modules (plugins) sont disponibles pour simuler des machines.

| Noms plugins                    | Fonctions                                                                     |
|---------------------------------|-------------------------------------------------------------------------------|
| [netns](docs/netns.md)          | Active un espace de nom réseau séparé                                         |
| [honeypot](docs/honeypot.md)    | Simuler des services (pour scan réseau)                                       |
| [execute](docs/execute.md)      | Démarrer un programme externe dans un espace de nom réseau séparé             |
| [lxc](docs/lxc.md)              | Démarrer un container lxc jetable à partir d'une image                        |
| [lxd](docs/lxd.md)              | Démarrer un container lxd jetable à partir d'une image                        |
| [docker](docs/docker.md)        | Démarrer un container docker jetable à partir d'une image                     |
| [kvm](docs/kvm.md)              | Démarrer une machine virtuelle jetable à partir d'une image                   |

# Monitor

Il est possible d'interagir avec le programme via la CLI.

Pour cela, exécuter avec l'option `--monitor`. Un socket unix sera créé :

```bash
sudo python3 archimulator.py -f examples/simple.yaml --monitor '/tmp/test.sock'
```

Il est également possible de forcer les droits lors de la création du socket unix :

```bash
sudo python3 archimulator.py -f examples/simple.yaml --monitor '/tmp/test.sock:root:utilisateur:660'
```

Pour se connecter à la CLI :
```bash
socat - unix-connect:/tmp/test.sock
```

Commande `help` pour lister les commandes :
```
> help
Commands :
	help - help message
	quit - quit cli
	stop - stop instance
	restart - restart instance
> quit
bye ...
```

**Avertissement : L'interface CLI n'est pas encore figée.**

# Substitution

Il est possible de modifier dynamiquement la configuration fournie par un fichier *yaml*. Par le exemple le fichier suivant :

```yaml
vlans :
    10 :
        intname: vlan10
        addresses : [ {{REPLACEME}}.1/24 ]
machines :
    machine-1 :
        netns :
            link : tap1
            addresses : [ {{REPLACEME}}.2/24 ]
            gw : {{REPLACEME}}.1
```

Chargé avec la commande suivante :
```bash
sudo python3 archimulator.py -f example.yaml -s '#{{REPLACEME}}#192.168.7#'
```

Fournira au simulateur d'architecture, la configuration suivante :
```yaml
vlans :
    10 :
        intname: vlan10
        addresses : [ 192.168.7.1/24 ]
machines :
    machine-1 :
        netns :
            link : tap1
            addresses : [ 192.168.7.2/24 ]
            gw : 192.168.7.1
```

Pour différentes substitutions, la syntaxe sera de la forme :
```bash
sudo python3 archimulator.py -f example.yaml -s '#{{REPLACEME_1}}#192.168.7#' '#{{REPLACEME_2}}#192.168.8#'
```

## Problèmes connus

### Protocole STP

Si le **protocole STP** est activé sur le *bridge* de la machine hôte, et que votre architecture est raccordée via une interface de type *external* à ce même switch, l'interface externe de l'architecture ne sera pas accessible, tant que la découverte de la nouvelle topologie ne sera pas finalisée (10 à 20 secondes environ).
