#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import threading

class AbstractMeta(threading.Thread):
    def __init__(self):
        self.services=[]

    def add(self, service):
        self.services.append(service)

    def start(self):
        for service in self.services:
            service.start()

    def stop(self):
        for service in self.services:
            service.stop()

    def join(self):
        for service in self.services:
            service.join()

