#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os, lxc, shutil
import configparser

from plugins.abstract.abstract_containers_plugins import AbstractContainersPlugin
from include.validate import validate_keys

class LxcPlugin(AbstractContainersPlugin):
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractContainersPlugin.__init__(self, "lc", "lxc-ls", current_instance, name, configuration, debug_enabled)
        self.do_renameInterface=False
        self.source_dir="/serve/lxc/images"
        self.tmp_dir="/serve/lxc/containers"
        self.container=None
        self.readConfigFile()

    def readConfigFile(self):
        self.debug("LxcPlugin:readConfigFile")
        plugin_configuration_file="lxc_plugin.conf"
        _etc_conf_file="/etc/archimulator/{}".format(plugin_configuration_file)
        _local_conf_file="{}/{}".format(os.path.join(os.path.dirname(__file__), 'lxc'), plugin_configuration_file)
        if os.path.exists(_etc_conf_file):
           plugin_configuration_file=_etc_conf_file
        elif os.path.exists(_local_conf_file):
           plugin_configuration_file=_local_conf_file
        else:
            raise Exception("Configuration file doesn't exist : {}\n".format(plugin_configuration_file))
        # read configuration file
        config = configparser.ConfigParser()

        try:
            config.read(plugin_configuration_file)
        except:
            raise Exception("Impossible to read configuration file {}".format(plugin_configuration_file))

        self.source_dir=config.get("PATH", "source_dir")
        self.tmp_dir=config.get("PATH", "tmp_dir")

    def execInContainer(self, cmd, wait=True, ignore_errors=False):
        if not self.containerStarted:
            return
        cmd_str=" ".join(cmd)
        self.debug(f"Exec cmd in lxc : '{cmd_str}'")
        if not wait:
            code=self.container.attach(lxc.attach_run_command, cmd)
        else:
            code=self.container.attach_wait(lxc.attach_run_command, cmd)
            # TODO FIXME KeyboardInterrupt no longer works from here
            if not ignore_errors and code!=0:
                raise Exception(f"Command return code error (!=0) code={code}")
        return code

    def getContainerPid(self):
        if self.currentPid:
            return self.currentPid
        try:
            self.currentPid=self.container.init_pid
            if self.currentPid>1:
                return self.currentPid
        except:
            pass
        raise Exception("Impossible to get PID of {}".format(self.current_container_name))

    def createContainer(self, image):
        source=lxc.Container(image, config_path=self.source_dir)
        if not source.defined:
            raise Exception(f"Source image {image} doesn't exist")

        self.container=lxc.Container(self.current_container_name, config_path=self.tmp_dir)
        if self.container.defined:
            raise Exception(f"Impossible to create lxc container {self.current_container_name} (from image '{image}') : Container already exists")
        
        self.container=source.clone(self.current_container_name, config_path=self.tmp_dir, bdevtype="btrfs", flags=lxc.LXC_CLONE_SNAPSHOT)
        if not self.container.defined:
            raise Exception(f"Impossible to create lxc container {self.current_container_name} (from image '{image}') : Copy failed")

        # remove default network interface
        try:
            self.container.network.remove(0)
        except:
            self.debug("Impossible to remove default interface (doesn't exist ?)")

        if "quotas" in self.configuration:
            self.setQuotas(self.configuration["quotas"])

        if "command" in self.configuration:
            command=self.configuration['command']
            if command and isinstance(command, list):
                    command=" ".join(command)
            self.container.set_config_item('lxc.init.cmd', command)
        if not self.container.save_config():
            raise Exception(f"Impossible to change configuration for lxc container {self.current_container_name}")

        self.containerCreated=True

    def startContainer(self):
        if not self.container.start():
            raise Exception(f"Impossible to start lxc container {self.current_container_name}")
        self.containerStarted=True     

    def isContainerExist(self):
        try:
            return self.container.defined
        except:
            pass
        return False

    def restartContainer(self):
        if self.containerStarted:
            # move interface in default network namespace before, otherwise it will be lost ...
            cmds=[]
            for configuration in self.configuration["interfaces"]:
                link, intname = self.getLinkAndIntNameFromConfig(configuration)
                cmds.append(self.commands.setDownInterface(intname))
                cmds.append(self.commands.renameInterface(intname, link))
                cmds.append(self.commands.attachIntToNetns(link, '1')) # to default netns
            for cmd, wait, _ in cmds:
                self.execCommandonNetns(self.current_container_name, cmd, wait,  ignore_error=True)

            # WARNING : do not use self.container.reboot(), but stop then start
            # because container.reboot doesn't wait, and reboot fails if first process is not 'init' (ex. entrypoint.sh ...)
            self.execCommand(('lxc-stop', '-P', self.tmp_dir, self.current_container_name, '-t', '4'))
            self.startContainer()

    def destroyContainer(self):
        if not self.containerStarted and self.containerCreated:
            # created but never started -> destroy
            self.container.destroy()
            return
        if self.containerStarted:
            self.container.stop()
            self.container.wait("STOPPED", 3)
        # fix if stopped and not deleted
        if self.isContainerExist():
            self.container.destroy()
        tmp_path_container=f"{self.tmp_dir}/{self.current_container_name}"
        if os.path.isdir(tmp_path_container):
            print(f"Warning - {self.current_container_name} still exist, try to remove directory")
            try:
                shutil.rmtree(tmp_path_container)
            except:
                print(f"Warning - Impossible to remove {tmp_path_container}")

        self.containerCreated=False
        self.containerStarted=False

    def pushFileInContainer(self, src, dst):
        if self.containerCreated:
            self.execCommand(('cp', '-rP', src, f'{self.tmp_dir}/{self.current_container_name}/rootfs/{dst}'))

    def attachInterfaceInContainerStage1(self, link, intname):
        pass

    def attachInterfaceInContainerStage2(self, link, intname):
        if not self.container.add_device_net(link, intname):
            raise Exception(f"Impossible to attach interface {link} to container {self.current_container_name}")

    def setQuotas(self, quotas):
        def normalize_Unit(value):
            return value[:-1] if value.endswith('B') else value

        validate_keys(('disk', 'cpus', 'memory', 'memory-swap'), quotas)
        if "disk" in quotas:
            disk=normalize_Unit(quotas["disk"])
            self.execCommand(('btrfs', 'qgroup', 'limit', f'{disk}', f'{self.tmp_dir}/{self.current_container_name}/rootfs'))
        if "cpus" in quotas:
            cpu=quotas["cpus"]*100000
            self.container.set_config_item('lxc.cgroup2.cpu.max', f'{cpu} 100000')
        if "memory" in quotas:
            memory=normalize_Unit(quotas["memory"])
            self.container.set_config_item('lxc.cgroup2.memory.max', f'{memory}')
        if "memory-swap" in quotas:
            memory_swap=normalize_Unit(quotas["memory-swap"])
            self.container.set_config_item('lxc.cgroup2.memory.swap.max', f'{memory_swap}')

pluginName=LxcPlugin

