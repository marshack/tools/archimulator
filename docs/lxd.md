### lxd

> **Important : LXD est déprécié dans Archimulator et sa prise en charge sera supprimée dans une prochaine version**
>
> La société Canonical a modifié le modèle de développement de LXD pour en faire un projet d'entreprise plutôt qu'un projet communautaire. De plus, LXD n'était plus disponible qu'au format de paquets Snap.
>
> Toutefois, vous pouvez facilement transformer vos images LXD en images LXC à l'aide du [script disponible](https://gitlab.com/marshack/tools/portailvms/-/blob/main/misc/tools/lxc/image_lxd2lxc.sh).


Ce plugin permet de démarrer un container jetable à partir d'une image, et de le raccorder à notre architecture.

Installation :
```bash
sudo apt install lxd python3-pylxd
sudo lxd init
```

> Il est recommandé d'utiliser un pool de stockage de type **btrfs** qui prend en charge CoW (*Copy on write*), ce qui permet d’accélérer la copie du système de fichiers lors de la création du container, et préserver l'espace de stockage.

Il est également possible de démarrer un programme ou un script dans le container.

* *image* : image de base du container (requis)
* *link* : interface réseau reliée au Switch (requis)
* *intname* :  nom de l'interface réseau dans le container (recommandé - généralement eth0, eth1, ...)
* *addresses* : liste d'adresses IP (optionnel)
* *gw* : passerelle par défaut (optionnel)
* *push* : copier un fichier, de la machine hôte vers le container (optionnel)
* *start* : programme lancé au démarrage, dans le container (optionnel). Ne doit pas être bloquant
* *command* : remplace la commande par défaut (optionnel)
* *quotas* : permet de limiter les ressources (optionnel)
* ...

```yaml
    machine2 :
        lxd :
            image : apache2
            interfaces:
                link : tap2
                intname : eth0
                addresses : [ 192.168.100.4/24 ]
                gw : 192.168.100.1
            push :
             - /etc/issue /etc/
             - /etc/aliases /etc/
            start : [ "/bin/echo", "start" ]
            command: [ "sleep", "infinity" ]
```

#### Quotas (optionnel)

Si l'on souhaite limiter les ressources pour un container lxd (exemple) :
```yaml
            quotas:
                cpus : 1
                cpu_allow: 10%
                disk: 300MB
                memory: 800MB
```

> **Important :** Se référer à la documentation officielle pour paramétrer correctement les quotas sous LXD : https://linuxcontainers.org/lxd/docs/master/instances

> Note : Afin d'éviter que le *daemon* lxc raccorde automatiquement le container sur le réseau par défaut, supprimer ce dernier et modifier le profil par défaut :
```bash
lxc profile device remove default eth0
lxc profile show default
lxc network delete lxdbr0
lxc network list
```
