# Contribuer

Merci d'envisager nous aider sur ce projet. Tout type de contribution est bienvenue

## J'ai une question

> Si vous souhaitez poser une question, nous supposons que vous avez lu la [documentation disponible](https://gitlab.com/marshack/tools/archimulator/-/blob/main/README.md).

Avant de poser une question, il est préférable de rechercher les [issues existantes](https://gitlab.com/marshack/tools/archimulator/-/issues) qui pourraient vous aider. Si vous avez trouvé un problème approprié et que vous avez encore besoin d'éclaircissements, vous pouvez écrire votre question en ajoutant un commentaire dans le ticket existant. Il est également conseillé de rechercher d'abord des réponses sur Internet.

Si vous ressentez toujours le besoin de poser une question et avez besoin d'éclaircissements, nous vous recommandons ce qui suit :

 - Créez [nouvelle issue](https://gitlab.com/marshack/tools/archimulator/-/issues/new)
 - Fournissez autant de contexte que possible sur ce que vous rencontrez (pile d'appels, message d'erreur, ...)
 - Fournissez les versions du projet et de la plate-forme (version os, architecture, etc.), en fonction de ce qui semble pertinent

## Je souhaite contribuer

> ### Mention légale
> Lorsque vous contribuez à ce projet, vous devez accepter que vous êtes l'auteur de 100% du contenu, que vous disposez des droits nécessaires sur le contenu et que le contenu que vous contribuez peut être fourni sous la licence du projet.
     
### Signaler un dysfonctionnement

#### Avant de soumettre un rapport de bogue

 - Assurez-vous que vous utilisez la dernière version
 - Déterminez si votre bogue est vraiment un bogue et non une erreur de votre côté, par ex. en utilisant des composants/versions d'environnement incompatibles (Assurez-vous d'avoir lu la [documentation](https://gitlab.com/marshack/tools/archimulator/-/blob/main/README.md)
 - Pour voir si d'autres utilisateurs ont rencontré (et potentiellement déjà résolu) le même problème que vous, vérifiez s'il n'y a pas déjà un rapport de bogue existant pour votre bogue ou erreur dans le [bug tracker](https://gitlab.com/marshack/tools/archimulator/-/issues/?label_name%5B%5D=bug)
- Assurez-vous également de rechercher sur Internet (y compris *Stack Overflow*) pour voir si des utilisateurs extérieurs à la communauté ont discuté du problème
- Collecter des informations sur le bug :
  - Suivi de la pile (Traceback)
  - Système d'exploitation, plate-forme et version (Linux debian 11, Linux Ubuntu 22.04, ...)
  - Version de l'interpréteur python, de l'environnement d'exécution, selon ce qui semble pertinent
  - Eventuellement votre entrée et la sortie
  - Pouvez-vous reproduire le problème de manière fiable ? Et pouvez-vous également le reproduire avec des versions plus anciennes ?

#### Comment soumettre un bon rapport de bogue ?

> Vous ne devez jamais signaler des problèmes, des vulnérabilités ou des bogues liés à la sécurité, y compris des informations sensibles, au traqueur de problèmes ou ailleurs en public. Au lieu de cela, les bogues sensibles doivent être envoyés par e-mail à security@marshack.fr

Nous utilisons le suivi de tickets de GitLab pour suivre les bogues et les erreurs. Si vous rencontrez un problème avec le projet :

 - Ouvrez un ticket. (Puisque nous ne pouvons pas être sûrs à ce stade s'il s'agit d'un bogue ou non, nous vous demandons de ne pas encore parler d'un bogue et de ne pas étiqueter le problème)
 - Expliquez le comportement auquel vous vous attendez et le comportement réel
 - Veuillez fournir autant de contexte que possible et décrire les étapes de reproduction que quelqu'un d'autre peut suivre pour recréer le problème par lui-même. Cela inclut généralement votre code. Pour de bons rapports de bogues, vous devez isoler le problème et créer un cas de test réduit
 - Fournissez les informations que vous avez recueillies dans la section précédente

### Suggestion d'améliorations

Cette section vous guide tout au long de la soumission d'une suggestion d'amélioration pour *Archimulator*, **incluant des fonctionnalités entièrement nouvelles et des améliorations mineures aux fonctionnalités existantes**. Suivre ces directives aidera les mainteneurs et la communauté à comprendre votre suggestion et à trouver des suggestions connexes.

#### Avant de soumettre une amélioration

 - Assurez-vous que vous utilisez la dernière version
 - Lisez attentivement la [documentation](https://gitlab.com/marshack/tools/archimulator/-/blob/main/README.md) et découvrez si la fonctionnalité est déjà couverte, peut-être par une configuration individuelle
 - Effectuez une [recherche](https://gitlab.com/marshack/tools/archimulator/issues) pour voir si l'amélioration a déjà été suggérée. Si c'est le cas, ajoutez un commentaire au problème existant au lieu d'en ouvrir un nouveau
 - Découvrez si votre idée correspond à la portée et aux objectifs du projet. A vous de monter un dossier solide pour convaincre les développeurs du projet du bien-fondé de cette fonctionnalité. Gardez à l'esprit que nous voulons des fonctionnalités qui seront utiles à la majorité de nos utilisateurs et pas seulement à un petit sous-ensemble. Si vous ne ciblez qu'une minorité d'utilisateurs, envisagez d'écrire une bibliothèque de modules complémentaires/plugins

#### Comment soumettre une bonne suggestion d'amélioration ?

Les suggestions d'amélioration sont suivies en tant que [issues GitLab](https://gitlab.com/marshack/tools/archimulator/issues).

 - Utilisez un **titre clair et descriptif** pour le problème afin d'identifier la suggestion
 - Fournissez une **description étape par étape de l'amélioration suggérée** avec autant de détails que possible
 - **Décrivez le comportement actuel** et **expliquez quel comportement vous attendiez à la place** et pourquoi. À ce stade, vous pouvez également indiquer les alternatives qui ne fonctionnent pas pour vous
 - Vous pouvez **inclure des captures d'écran et des GIF animés** qui vous aident à démontrer les étapes ou à indiquer la partie à laquelle la suggestion est liée. Vous pouvez utiliser [cet outil](https://www.cockos.com/licecap/) pour enregistrer des GIF sur macOS et Windows, et [cet outil](https://github.com/colinkeenan/silentcast) ou [cet outil](https://gitlab.gnome.org/Archive/byzanz) sous Linux.
 - **Expliquez pourquoi cette amélioration serait utile** à la plupart des utilisateurs d'*Archimulator*. Vous pouvez également signaler les autres projets qui l'ont mieux résolu et qui pourraient servir d'inspiration

