#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

from pylxd import Client as ClientLxd

from plugins.abstract.abstract_containers_plugins import AbstractContainersPlugin
from include.validate import validate_keys

class LxdPlugin(AbstractContainersPlugin):
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractContainersPlugin.__init__(self, "ld", "lxd", current_instance, name, configuration, debug_enabled)
        self.do_renameInterface=False
        self.storage_name="default"
        try:
            self.client_lxd=ClientLxd()
        except:
            raise Exception("LXD client connection failed")
        self.container=None

    def execInContainer(self, cmd, wait=True, ignore_errors=False):
        if not self.containerStarted:
            return
        cmd_str=" ".join(cmd)
        self.debug(f"Exec cmd in lxd : '{cmd_str}'")
        if not wait:
            new_cmd=['lxc', 'exec', self.current_container_name , '--']
            new_cmd.extend(('nohup', 'sh', '-c', '{}'.format(cmd_str)))
            code, _=self.execCommand(new_cmd, wait, ignore_errors)
        else:
            try:
                code, _,stderr=self.container.execute(cmd)
                if not ignore_errors and code!=0:
                    raise Exception("Command return code error (!=0) code={}, stderr='{}'".format(code, stderr[:25]))
            except:
                if not ignore_errors:
                    raise Exception("Impossible to execute command in lxd container : {}".format(cmd_str))
        return code

    def getContainerPid(self):
        if self.currentPid:
            return self.currentPid
        try:
            self.currentPid=self.container.state().pid
            return self.currentPid
        except:
            pass
        raise Exception("Impossible to get PID of {}".format(self.current_container_name))

    def createContainer(self, image):
        config = {
            'name': self.current_container_name,
            'source': {'type': 'image', 'alias': image},
            'devices': {'root': {'path': '/', 'pool': self.storage_name, 'type': 'disk'}},
            'ephemeral': True
        }
        try:
            self.container=self.client_lxd.containers.create(config, wait=True)
        except Exception as err:
            self.containerCreated=False
            raise Exception("Impossible to create lxd container {0} (from image '{1}') : {2}".format(self.current_container_name, image, err))

        if "command" in self.configuration:
            command=self.configuration['command']
            if command and isinstance(command, list):
                    command=" ".join(command)
            self.container.config['raw.lxc']=f"lxc.init.cmd={command}"
            self.container.save(wait=True)
        if "quotas" in self.configuration:
            self.setQuotas(self.configuration["quotas"])
        self.containerCreated=True

    def startContainer(self):
        try:
            self.container.start(wait=True)
            self.containerStarted=True
        except Exception as err:
            self.containerStarted=False
            raise Exception("Impossible to start lxd container ({0}) : {1}".format(self.current_container_name, err))

    def isContainerExist(self):
        try:
            self.client_lxd.containers.get(self.current_container_name)
            return True
        except:
            pass
        return False

    def restartContainer(self):
        if self.containerStarted:
            self.container.restart(force=True, wait=True)

    def destroyContainer(self):
        if not self.containerStarted and self.containerCreated:
            # created but never started -> destroy
            self.container.delete()
            return
        if self.containerStarted:
            self.container.stop(force=True, wait=True)
        # fix if stopped and not deleted
        if self.isContainerExist():
            self.container.delete()
        self.containerCreated=False
        self.containerStarted=False

    def pushFileInContainer(self, src, dst):
        if self.containerCreated:
            if hasattr(self.container.files, 'recursive_put') and callable(getattr(self.container.files,'recursive_put')):
                # pylxd >= 2.2.7
                self.container.files.recursive_put(src, dst)
            else:
                cmd=('lxc', 'file', 'push', '-r', src, f"{self.current_container_name}/{dst}")
                self.execCommand(cmd)

    def attachInterfaceInContainerStage1(self, link, intname):
        self.container.devices[intname]={
            'name': intname,
            'nictype': 'physical',
            'parent': link,
            'type': 'nic'
        }
        self.container.save(wait=True)

    def attachInterfaceInContainerStage2(self, link, intname):
        pass

    def setQuotas(self, quotas):
        validate_keys(('disk', 'cpus', 'cpu_allow', 'memory'), quotas)
        if "disk" in quotas:
            self.container.devices['root']['size']=str(quotas["disk"])
        if "cpus" in quotas:
            self.container.config['limits.cpu']=str(quotas["cpus"])
        if "cpu_allow" in quotas:
            self.container.config['limits.cpu.allowance']=str(quotas["cpu_allow"])
        if "memory" in quotas:
            self.container.config['limits.memory']=str(quotas["memory"])
        self.container.save(wait=True)

pluginName=LxdPlugin

