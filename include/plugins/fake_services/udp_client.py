#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


import socket
import time

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
while True:
    time.sleep(8)
    client_socket.sendto(b"Hello, World!", ("192.168.100.2", 5425))

