#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os
import configparser
from abstract_system import AbstractSystem
from plugins.abstract.abstract_plugin import AbstractPlugin,TapType
from plugins.kvm.kvm_engine import KvmEngine
from include.validate import validate_keys

class KvmPlugin(AbstractPlugin, AbstractSystem):
    # kvm use tuntap interface
    taptype=TapType.tuntap
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractPlugin.__init__(self, current_instance, name, configuration, debug_enabled)
        AbstractSystem.__init__(self, debug_enabled)
        self.kvm_engine=None
        # default path
        self.configuration_path="./configurations/"
        self.target_disk="/serve/tmp/"
        self.tmp_dir="/tmp"
        self.internal_keys=['interfaces', 'image', 'link', 'mac', 'vnc']
        self.readConfigFile()

    def readConfigFile(self):
        self.debug("KvmPlugin:readConfigFile")
        plugin_configuration_file="kvm_plugin.conf"
        _etc_conf_file="/etc/archimulator/{}".format(plugin_configuration_file)
        _local_conf_file="{}/{}".format(os.path.join(os.path.dirname(__file__), 'kvm'), plugin_configuration_file)
        if os.path.exists(_etc_conf_file):
           plugin_configuration_file=_etc_conf_file
        elif os.path.exists(_local_conf_file):
           plugin_configuration_file=_local_conf_file
        else:
            raise Exception("Configuration file doesn't exist : {}\n".format(plugin_configuration_file))
        # read configuration file
        config = configparser.ConfigParser()

        try:
            config.read(plugin_configuration_file)
        except:
            raise Exception("Impossible to read configuration file {}".format(plugin_configuration_file))

        self.configuration_path=config.get("PATH", "configurations")
        self.target_disk=config.get("PATH", "target_disk")
        self.tmp_dir=config.get("PATH", "tmp_dir")
        if not os.path.exists(self.configuration_path):
            # relative path ?
            p=os.path.abspath(os.path.join(os.path.dirname(__file__),"../..", self.configuration_path))
            if os.path.exists(p) and os.path.isdir(p):
                self.configuration_path=p

    def checkPrerequisite(self):
        if os.path.exists("/dev/kvm"):
            program_name="kvm"
        else:
            program_name="qemu-system-x86_64"
        try:
            import subprocess
            subprocess.call((program_name, "--version"), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except OSError:
            raise Exception("Command {0} missing ({0} package is required)".format(program_name))

    def start(self):
        self.debug("KvmPlugin:start")
        self.checkPrerequisite()
        validate_keys(self.internal_keys, self.configuration)

        try:
            image_name=self.configuration["image"]
            configuration_filename="{}/{}.yaml".format(self.configuration_path, image_name)
            if not os.path.exists(configuration_filename):
                raise Exception("yaml file doesn't exist : {}".format(configuration_filename))
            links=[]
            if "interfaces" in self.configuration:
                interfaces=self.configuration["interfaces"]
                if isinstance(interfaces, dict):
                    interfaces=[interfaces]
                for interface in interfaces:
                    validate_keys(('link', 'mac'), interface)
                    if "link" in interface:
                        if "mac" in interface:
                            links.append((interface["link"], interface["mac"]))
                        else:
                            links.append(interface["link"])
            elif "link" in self.configuration:
                if "mac" in self.configuration:
                    links.append((self.configuration["link"], self.configuration["mac"]))
                else:
                    links=[self.configuration["link"]]

            if len(links)==0: raise Exception("'link' is required")

            # VNC
            vnc=None
            vnc_password=None
            if "vnc" in self.configuration:
                vnc_conf=self.configuration["vnc"]
                validate_keys(('listen', 'port', 'password'), vnc_conf)
                if "listen" in vnc_conf:
                    vnc=vnc_conf["listen"]
                if "port" in vnc_conf:
                    vnc="{}:{}".format(vnc, vnc_conf["port"])
                else: # default port 5901
                    vnc+=":1"
                if "password" in vnc_conf:
                    vnc+=",password=on"
                    vnc_password=vnc_conf["password"]

            netns_name="{}-ns-br".format(self.current_instance)
        except KeyError as e:
            raise Exception("arg {} is required".format(e.args[0]))

        # start KVM Instance
        self.kvm_engine=KvmEngine(configuration_filename, self.target_disk, \
            self.tmp_dir, \
            netns=netns_name, tapnamelist=links, vnc=vnc, \
            vnc_password=vnc_password, debug_enabled=self.debug_enabled)
        self.kvm_engine.start()

    def stop(self):
        self.debug("KvmPlugin:stop")
        if self.kvm_engine:
            self.kvm_engine.stop()
            self.kvm_engine.join()
        AbstractSystem.cleanAll(self)
        self.kvm_engine=None

    def restart(self):
        self.debug("KvmPlugin:restart")
        if self.kvm_engine:
            self.kvm_engine.restartVM()

pluginName=KvmPlugin
