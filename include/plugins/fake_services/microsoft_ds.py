#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

from wrapper_services import WrapperServices

class MicrosoftDs():
    banner=None
    sig=b"\x00\x00\x00\x75\xff\x53\x4d\x42\x72\x00\x00\x00\x00\x88\x01\x40\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x40\x06\x00\x00\x01\x00\x11\x07\x00\x03\x32\x00\x01\x00\x04\x11\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\xfc\xe3\x01\x00\x0c\xf6\x3f\x8e\xd6\x52\xd6\x01\x88\xff\x08\x30\x00\x3b\xea\x89\xd0\x35\x9f\x6b\xd1\x57\x00\x4f\x00\x52\x00\x4b\x00\x47\x00\x52\x00\x4f\x00\x55\x00\x50\x00\x00\x00\x44\x00\x41\x00\x4e\x00\x49\x00\x45\x00\x4c\x00\x2d\x00\x50\x00\x43\x00\x00\x00"

if __name__ == '__main__':
    def signal_handler(signal, frame):
        service.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    service=WrapperServices(port=445, banner=MicrosoftDs.banner, sig=MicrosoftDs.sig, debug_mode=True)
    service.start()
    service.join()
