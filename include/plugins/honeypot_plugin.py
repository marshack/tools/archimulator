#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os

from plugins.abstract.abstract_netns_plugin import AbstractNetnsPlugin

class HoneypotPlugin(AbstractNetnsPlugin):
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractNetnsPlugin.__init__(self, current_instance, name, configuration, debug_enabled)
        self.internal_keys.append('services')

    def startService(self, service):
        fake_services_dir = os.path.join(os.path.dirname(__file__), 'fake_services')
        filename="{}/{}.py".format(fake_services_dir, service)
        if not os.path.isfile(filename):
            self.printError("Honeypot doesn't exist (script {} missing)".format(filename))
            return
        self.execCommandonNetns(self.netns_name, ("python3", filename), wait=False)

    def start(self):
        self.debug("HoneypotPlugin:start")
        AbstractNetnsPlugin.start(self)

        services=self.configuration["services"]
        if not isinstance(services, list):
            raise Exception("No service found (it's not a list)")

        for service in services:
            self.startService(service)


    def stop(self):
        self.debug("HoneypotPlugin:stop")
        AbstractNetnsPlugin.stop(self)

pluginName=HoneypotPlugin
