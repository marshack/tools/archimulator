#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys


class TapType():
    veth=0
    tuntap=1

class TypeInterface():
    internal=0
    external=1

# Plugin API
class AbstractPlugin():
    # default tap type interface
    taptype=TapType.veth
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        self.current_instance=current_instance
        self.name=name
        self.configuration=configuration
        self.debug_enabled=debug_enabled
        self.internal_keys=[]

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def printError(self, msg):
        sys.stderr.write("ERROR - {}\n".format(msg))

    def start(self):
        print("REIMPLEMENT ME")

    def stop(self):
        print("REIMPLEMENT ME")

    def restart(self):
        print("REIMPLEMENT ME")

pluginName=AbstractPlugin
