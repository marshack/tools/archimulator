#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os
from abstract_system import AbstractSystem
from plugins.abstract.abstract_plugin import AbstractPlugin
from include.validate import validate_keys

class AbstractContainersPlugin(AbstractPlugin, AbstractSystem):
    def __init__(self, prefix, program_name, current_instance, name, configuration, debug_enabled=False):
        AbstractPlugin.__init__(self, current_instance, name, configuration, debug_enabled)
        AbstractSystem.__init__(self, debug_enabled)
        self.prefix=prefix
        self.program_name=program_name
        self.current_container_name=None
        self.containerCreated=False
        self.containerStarted=False
        self.currentPid=None
        self.do_renameInterface=False
        self.internal_keys=['interfaces', 'image' ,'start' , 'command', 'push', 'routes', 'quotas']

    def checkPrerequisite(self):
        try:
            import subprocess
            subprocess.call([self.program_name], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except OSError:
            raise Exception("Command {0} missing ({0} package is required)".format(self.program_name))

    def execInContainer(self, cmd, wait=True, ignore_errors=False):
        print("REIMPLEMENT ME")

    def createContainer(self, image):
        print("REIMPLEMENT ME")

    def getContainerPid(self):
        return self.currentPid

    def startContainer(self):
        print("REIMPLEMENT ME")

    def isContainerExist(self):
        print("REIMPLEMENT ME")

    def destroyContainer(self):
        print("REIMPLEMENT ME")

    def restartContainer(self):
        print("REIMPLEMENT ME")

    def pushFileInContainer(self, src, dst):
        print("REIMPLEMENT ME")

    def netCmdInContainer(self, cmd, wait=True, ignore_errors=False):
        # exec command in the network namespace for this container
        pid=self.getContainerPid()
        netns_exist=False
        try:
            netns_exist=os.path.exists(os.readlink('/var/run/netns/{}'.format(self.current_container_name)))
        except: pass
        if pid and netns_exist:
            self.execCommandonNetns(self.current_container_name, cmd, wait, ignore_errors)
        else:
            # fallback method
            self.execInContainer(cmd, wait, ignore_errors)

    def attachInterfaceInContainerStage1(self, link, intname):
        # before container start
        print("REIMPLEMENT ME")

    def attachInterfaceInContainerStage2(self, link, intname):
        # after container started
        print("REIMPLEMENT ME")

    def removeInterface(self, interface):
        self.debug("AbstractContainersPlugin:removeInterface : {}".format(interface))
        cmd, wait, _=self.commands.removeInterface(interface)
        # continue, if error (interface exist or not)
        self.netCmdInContainer(cmd, wait, ignore_errors=True)

    def getLinkAndIntNameFromConfig(self, configuration):
        link="{}-B-{}".format(configuration["link"], self.current_instance)
        if "intname" in configuration:
            intname=configuration["intname"]
        else:
            intname=configuration["link"]
        return link, intname

    def removeLinkOfNetNS(self):
        ln='/var/run/netns/{}'.format(self.current_container_name)
        if os.path.islink(ln):
            try:
                os.remove(ln)
                self.debug(f"remove symlink : {ln}")
            except:
                self.printError(f"Impossible to remove symlink : {ln}")
            if self.current_container_name in self.netns_collection:
                self.netns_collection.remove(self.current_container_name)

    def createLinkNetNsIfNotExist(self):
        if not self.isNetnsExist(self.current_container_name):
            pid=self.getContainerPid()
            # create link for netns
            src='/proc/{}/ns/net'.format(pid)
            dst='/var/run/netns/{}'.format(self.current_container_name)
            if pid:
                try:
                    os.symlink(src, dst)
                    self.debug(f"Create symlink : {dst} -> {src}")
                    self.netns_collection.append(self.current_container_name)
                except:
                    self.printError(f"Impossible to create symlink : {dst} -> {src}")

    def configureContainerAfterStarted(self):
        self.createLinkNetNsIfNotExist()
        # up loopback
        cmds=[(self.commands.setUpInterface("lo"))]
        for configuration in self.configuration["interfaces"]:
            link, intname = self.getLinkAndIntNameFromConfig(configuration)
            # attach interface - stage 2
            self.attachInterfaceInContainerStage2(link, intname)
            if self.do_renameInterface:
                # remove origin interface, if exist
                self.removeInterface(intname)

            cmds.extend(self.getCmdsToConfigureInterface(configuration, \
                link, rename_interface=self.do_renameInterface))

        if "routes" in self.configuration:
            configuration_routes=self.configuration["routes"]
            cmds.extend(self.getCmdsToConfigureRoutes(configuration_routes))

        # configure network
        for cmd, wait, ignore_errors in cmds:
            self.netCmdInContainer(cmd, wait, ignore_errors)

        if "start" in self.configuration:
            start_cmd=self.configuration["start"]
            if not isinstance(start_cmd, list):
                raise Exception("No start command found (it's not a list)")
            self.execInContainer(start_cmd, True, False)

    def start(self):
        self.debug("AbstractContainersPlugin:start")
        self.checkPrerequisite()
        valid_keys=self.internal_keys+list(self.interface_valid_keys)
        validate_keys(valid_keys, self.configuration)

        try:
            image=self.configuration["image"]
            # create container
            self.current_container_name="{}-{}-{}".format(
                self.prefix,
                self.current_instance,
                self.name)

            if isinstance(self.configuration["interfaces"], dict):
                    self.configuration["interfaces"]=[self.configuration["interfaces"]]

            self.createContainer(image)

            for configuration in self.configuration["interfaces"]:
                link, intname = self.getLinkAndIntNameFromConfig(configuration)
                # attach interface - stage 1
                self.attachInterfaceInContainerStage1(link, intname)

            if "push" in self.configuration:
                lst=self.configuration["push"]
                if isinstance(lst, str):
                    lst=[lst]
                for l in lst:
                    src,dst=l.split(' ')
                    self.pushFileInContainer(src,dst)

            self.startContainer()
            self.configureContainerAfterStarted()

        except KeyError as e:
            raise Exception("arg {} is required".format(e.args[0]))

    def stop(self):
        self.debug("AbstractContainersPlugin:stop")
        try:
            self.destroyContainer()
        except Exception as e:
            self.printError("when stopping '{}'".format(e.args[0]))
        AbstractSystem.cleanAll(self)

    def restart(self):
        self.debug("AbstractContainersPlugin:restart")
        try:
            self.restartContainer()
            # pid change when restart. Force to recreate symlink
            self.removeLinkOfNetNS()
            self.currentPid=None
            self.configureContainerAfterStarted()
        except Exception as e:
            self.printError("when restarting '{}'".format(e.args[0]))

pluginName=AbstractContainersPlugin

