### execute

Permet de démarrer un programme externe dans un espace de nom réseau séparé. Celui-ci ne doit pas être bloquant.

* *link* : interface réseau (requis)
* *start* : programme externe lancé au démarrage (requis). Ne doit pas être bloquant
* *stop* : programme externe à exécuter lors de l'arrêt (optionnel)
* *addresses* : liste d'adresses IP (optionnel)
* *gw* : passerelle par défaut (optionnel)
* ...

 ```yaml
    machine-1 :
        execute :
            link : tap1
            start : [ "/bin/echo", "start" ]
            stop : [ "/bin/echo", "stop" ]
 ```
