#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


from abstract_system import AbstractSystem
from plugins.abstract.abstract_plugin import AbstractPlugin
from include.validate import validate_keys

class AbstractNetnsPlugin(AbstractPlugin, AbstractSystem):
    def __init__(self, current_instance, name, configuration, debug_enabled=False):
        AbstractPlugin.__init__(self, current_instance, name, configuration, debug_enabled)
        AbstractSystem.__init__(self, debug_enabled)
        self.netns_name=None
        self.link=""
        self.internal_keys=['interfaces', 'routes']

    def start(self):
        valid_keys=self.internal_keys+list(self.interface_valid_keys)
        validate_keys(valid_keys, self.configuration)
        try:
            if "interfaces" in self.configuration:
                configuration_interfaces=self.configuration["interfaces"]
            else:
                configuration_interfaces=self.configuration.copy()
                # keep internal keys
                for k in self.internal_keys:
                    if k in configuration_interfaces:
                        configuration_interfaces.pop(k)

            if isinstance(configuration_interfaces, dict):
                    configuration_interfaces=[configuration_interfaces]
            for configuration in configuration_interfaces:
                self.link="{}-B-{}".format(configuration["link"],self.current_instance)
                self.netns_name="{}-{}".format(self.current_instance, self.name)
                # attach to namespace
                self.attachIntToNetns(self.link, self.netns_name)
                self.configureInterface(configuration, self.link, self.netns_name)

            if "routes" in self.configuration:
                configuration_routes=self.configuration["routes"]
                self.configureRoutes(configuration_routes, self.netns_name)

        except KeyError as e:
            raise Exception("arg {} is required".format(e.args[0]))

    def stop(self):
        AbstractSystem.cleanAll(self)

    def restart(self):
        pass # no restart required for netns

