#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


from abstract_ssl_service import AbstractSSLservice
from abstract_service import AbstractService
from abstract_clientsocket import AbstractClientSocket

class WrapperSocket(AbstractClientSocket):
    def __init__(self, conn, debug_mode=False):
        AbstractClientSocket.__init__(self, conn, debug_mode)
        self.banner=None
        self.sig=None

    def setBanner(self, banner):
        self.banner=banner

    def setSignature(self, sig):
        self.sig=sig

    def bannerHandler(self):
        if self.banner:
            self.sendDatas(self.banner)

    def readyReadHandler(self, data):
        if self.sig:
            self.sendDatas(self.sig)


class _AbstractWrapperService():
    def __init__(self, debug_mode=False):
        self.debug_mode=debug_mode
        self.banner=None
        self.sig=None

    def setBanner(self, banner):
        self.banner=banner

    def setSignature(self, sig):
        self.sig=sig

# TLS/SSL
class _WrapperSSLService(AbstractSSLservice, _AbstractWrapperService):
    def __init__(self, host='0.0.0.0', port=2222, debug_mode=False):
        AbstractSSLservice.__init__(self, host=host, port=port, debug_mode=debug_mode)
        _AbstractWrapperService.__init__(self, debug_mode=debug_mode)

    def createSocketHandler(self, sock):
        s=WrapperSocket(sock, self.debug_mode)
        s.setBanner(self.banner)
        s.setSignature(self.sig)
        return s

# Plain text
class _WrapperService(AbstractService, _AbstractWrapperService):
    def __init__(self, host='0.0.0.0', port=8080, debug_mode=False):
        AbstractService.__init__(self, host=host, port=port, debug_mode=debug_mode)
        _AbstractWrapperService.__init__(self, debug_mode=debug_mode)

    def createSocketHandler(self, sock):
        s=WrapperSocket(sock, self.debug_mode)
        s.setBanner(self.banner)
        s.setSignature(self.sig)
        return s

class WrapperServices():
    def __init__(self, host='0.0.0.0', port=8080, with_ssl=False, banner=None, sig=None, debug_mode=False):
        self.host=host
        self.port=port
        self.with_ssl=with_ssl
        self.debug_mode=debug_mode
        self.banner=banner
        self.sig=sig
        self.server=None

    def setBanner(self, banner):
        self.banner=banner

    def setSignature(self, sig):
        self.sig=sig

    def start(self):
        if self.with_ssl:
            self.server=_WrapperSSLService(self.host, self.port, self.debug_mode)
        else:
            self.server=_WrapperService(self.host, self.port, self.debug_mode)
        if self.banner:
            self.server.setBanner(self.banner)
        if self.sig:
            self.server.setSignature(self.sig)
        self.server.start()

    def stop(self):
        if self.server:
            self.server.stop()

    def join(self):
        if self.server:
            self.server.join()

if __name__ == '__main__':
    def signal_handler(signal, frame):
        service1.stop()
        service2.stop()

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)
    
    banner=b"220 ProFTPD 1.2.8 Server\nName: "
    service1=WrapperServices(port=21, banner=banner, debug_mode=True)
    service2=WrapperServices(port=990, with_ssl=True, banner=banner, debug_mode=True)
    service1.start()
    service2.start()
    service1.join()
    service2.join()
