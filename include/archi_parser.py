#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os, subprocess, re
import threading, datetime, uuid
import yaml

sys.path.append('include')
from include.bridge import Bridge
from include.plugins_manager import PluginsManager
from include.cli import Cli
from include.validate import validate_keys

class Architecture(threading.Thread):
    def __init__(self, filename, version="unknown", sockfilename=None, substitute=None, debug_enabled=False, quiet=False):
        threading.Thread.__init__(self)
        self.conf_filename=filename
        self.substitute=substitute
        self.debug_enabled=debug_enabled
        self.quiet=quiet
        self._type_of_tap_from_plugin={}
        self._type_of_tap_interface={}
        self.arch_yaml=None
        self.bridge=None
        self.retcode=0
        self.lockTerminated = threading.Lock()
        self.stop_requested = threading.Event()
        self.instance_ready = threading.Event()
        self.tmp_id_lock_file=None
        self.current_instance=self.getUniqueID()
        self.plugins_manager=PluginsManager(self.current_instance, debug_enabled)
        self.cli=None
        if sockfilename:
            self.cli=Cli(sockfilename, version, self.stop, self.restart, debug_enabled)
            self.cli.start()

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def printError(self, msg):
        sys.stderr.write("ERROR - {}\n".format(msg))

    def printMessage(self, msg):
        if not self.quiet:
            print(msg)

    def fatalError(self, msg=None):
        if msg:
            self.printError(msg)
        self.cleanAll()
        self.retcode=1
        sys.exit(1)

    def generateShortUUID(self):
        return uuid.uuid4().hex[:5]

    def getUniqueID(self):
        tmp_path="/tmp/archimulator/lock"
        if not os.path.isdir(tmp_path):
            try:
                os.makedirs(tmp_path, 0o750)
            except:
                self.fatalError("Impossible to create directory : {}".format(tmp_path))
                return
        for _ in range(0, 20):
            tmp_id=self.generateShortUUID()
            tmp_id_lock_file="{}/archi_{}.lock".format(tmp_path, tmp_id)
            if not os.path.exists(tmp_id_lock_file):
                # create lock file
                os.mknod(tmp_id_lock_file)
                self.tmp_id_lock_file=tmp_id_lock_file
                return tmp_id

    def removeLockFile(self):
        if self.tmp_id_lock_file:
            try:
                # remove lock file
                os.remove(self.tmp_id_lock_file)
            except:
                self.printError("Impossible to remove lock file {}".format(self.tmp_id_lock_file))

    def substitution(self, conf):
        if self.substitute and isinstance(self.substitute, list):
            for sub in self.substitute:
                delim=sub[0]
                if sub.count(delim) >= 3:
                    t=sub.split(delim)
                    pattern=t[1]
                    repl=t[2]
                    try:
                        conf=re.sub(pattern, repl, conf)
                        continue
                    except:
                        pass
                sys.stderr.write("Warning - Impossible to perform substitution ({})\n".format(sub))
        return conf

    def readYamlFile(self):
        self.debug('Architecture:readYamlFile')
        try:
            with open(self.conf_filename) as f:
                return yaml.safe_load(self.substitution(f.read()))
        except OSError as e:
            self.printError("{} (filename : {})".format(e.strerror, self.conf_filename))
        except Exception as e:
            self.printError("Impossible to parse yaml file {}.\n\nDetail :\n{}\n".format(self.conf_filename, str(e)))
        return None

    def update_bridge_config_with_taptype(self, bridge):
        self.debug('Architecture:update_bridge_config_with_taptype')
        try:
            interfaces=bridge["interfaces"]
            for key, interface in interfaces.items():
                if 'link' in interface:
                    if 'type' in interface and interface['type']=='external':
                        continue
                    link=interface['link']
                    if link in self._type_of_tap_interface:
                        bridge['interfaces'][key]['taptype']=self._type_of_tap_interface[link]
        except KeyError as e:
            raise Exception("arg {} is required".format(e.args[0]))

    def createBridges(self):
        self.debug('Architecture:createBridges')
        self.retrieve_taptype_from_plugins()
        self.update_bridge_config_with_taptype(self.switch)
        self.bridge=Bridge(self.switch, self.virtual_interfaces, self.routes, self.current_instance, self.debug_enabled)
        self.bridge.start()

    def createMachines(self):
        self.debug('Architecture:createMachines')
        try:
            if not isinstance(self.machines, dict):
                raise Exception("No machine configuration found")

            for machine_name, plugins in self.machines.items():
                if not isinstance(plugins, dict):
                    raise Exception("No plugin configuration found")

                for plugin_name, configuration in plugins.items():
                    self.plugins_manager.loadPlugin(plugin_name, machine_name, configuration)

        except KeyError as e:
            raise Exception("arg {} is required".format(e.args[0]))

    def update_tap_type_dicts(self, plugin_name, interface):
        if plugin_name in self._type_of_tap_from_plugin:
            # in cache
            _type=self._type_of_tap_from_plugin[plugin_name]
        else:
            _type=self.plugins_manager.getTypeInterfaceFromPluginName(plugin_name)
            self._type_of_tap_from_plugin[plugin_name]=_type
        self._type_of_tap_interface[interface]=_type

    def retrieve_taptype_from_plugins(self):
        self.debug('Architecture:retrieve_taptype_from_plugins')
        try:
            if not isinstance(self.machines, dict):
                raise Exception("No machine configuration found")

            for _, plugins in self.machines.items():
                if not isinstance(plugins, dict):
                    raise Exception("No plugin configuration found")
                for plugin_name, configuration in plugins.items():
                    if 'link' in configuration:
                        self.update_tap_type_dicts(plugin_name, configuration["link"])
                    if 'interfaces' in configuration:
                        interfaces=configuration["interfaces"]
                        if isinstance(interfaces, dict):
                            interfaces=[interfaces]
                        for interface in interfaces:
                            if 'link' in interface:
                                self.update_tap_type_dicts(plugin_name, interface["link"])

        except KeyError as e:
            raise Exception("arg {} is required".format(e.args[0]))

    def cleanAll(self):
        self.debug('Architecture:cleanAll')
        try:
            self.plugins_manager.stopAll()
        except Exception as e:
            self.printError(e.args[0])
        try:
            if self.bridge:
                self.bridge.stop()
        except Exception as e:
            self.printError(e.args[0])
        try:
            if self.cli:
                self.cli.stop()
        except Exception as e:
            self.printError(e.args[0])
        self.removeLockFile()

    def stop(self):
        self.debug('Architecture:stop')
        self.stop_requested.set()
        if self.lockTerminated.locked():
            self.lockTerminated.release()

    def restart(self):
        self.debug('Architecture:restart')
        if self.instance_ready.is_set(): # only if is ready
            self.instance_ready.clear() # block other restart request
            self.plugins_manager.restartAll()
            self.instance_ready.set()

    def run(self):
        started_time=datetime.datetime.now()
        self.printMessage("Creating ...")
        if not os.path.isfile(self.conf_filename):
            self.fatalError("yaml file doesn't exist : {}".format(self.conf_filename))
        try:
            subprocess.call(["bridge"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except OSError:
            self.fatalError("Command bridge missing (iproute2 required)")

        self.arch_yaml=self.readYamlFile()
        if not self.arch_yaml:
            self.fatalError()
        try:
            validate_keys(('archi', 'switch', 'vlans', 'routes', 'machines'), self.arch_yaml)
            self.archi=self.arch_yaml["archi"]
            validate_keys(('name', 'version', 'description'), self.archi)
            self.archi_name=self.archi["name"]
            self.switch=self.arch_yaml["switch"]
            self.virtual_interfaces=None
            if "vlans" in self.arch_yaml:
                self.virtual_interfaces=self.arch_yaml["vlans"]
            self.routes=None
            if "routes" in self.arch_yaml:
                self.routes=self.arch_yaml["routes"]

            self.machines=self.arch_yaml["machines"]
        except KeyError as e:
            self.fatalError("arg {} is required (missing in yaml file)".format(e.args[0]))

        try:
            self.createBridges()
            self.createMachines()
        except Exception as e:
            self.fatalError(e)

        self.lockTerminated.acquire()
        count_time=datetime.datetime.now()-started_time
        self.printMessage("Creation complete after {}s".format(count_time.seconds))
        self.printMessage("Instance : {}".format(self.current_instance))
        if not self.stop_requested.is_set():
            self.instance_ready.set() # instance is now ready
            self.lockTerminated.acquire() # lock until release
        started_time=datetime.datetime.now()
        self.printMessage("Destroying...")
        self.cleanAll()
        count_time=datetime.datetime.now()-started_time
        self.printMessage("Destruction complete after {}s".format(count_time.seconds))

if __name__ == '__main__':
    import signal
    def signal_handler(signal, frame):
        arch.stop()

    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    arch=Architecture("../examples/simple.yaml", debug_enabled=True)
    arch.start()
    arch.join()

