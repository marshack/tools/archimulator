# Changelog


## [1.1.0] - 2024-03-08
 - Support d'IPv6. Ajout de la clé 'accept-ra'
 - Ajout de la clé 'command' pour LXC et LXD
 - Support d'UEFI pour KVM
 - Fix: L'option booléenne 'password' de qemu est dépréciée
 - De nombreuses corrections de bugs/améliorations


## [1.0.2] - 2023-02-02

 - Premier commit (ouverture du code)

